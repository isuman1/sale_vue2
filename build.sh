NOW=$(date +"%d/%m/%Y %R")
json -I -f ./src/service/version.json  -e "this.apibase='$1'"
json -I -f ./src/service/version.json  -e "this.date='$NOW'"
echo $(git log -1 HEAD --pretty=format:%s)
commit=$(git log -1 HEAD --pretty=format:%s)
echo $commit
IFS=":" read -ra ADDR <<< "${commit}"; echo ${ADDR[0]}
echo ${ADDR[0]}
if [ ${ADDR[0]} == "feat" ]
then
    echo 1
    json -I -f ./src/service/version.json  -e "this.minor=(this.minor)+1"
elif [ ${ADDR[0]} == "major" ]
then
    echo 2
    json -I -f ./src/service/version.json  -e "this.major=(this.major)+1"
else
    echo 3
    json -I -f ./src/service/version.json  -e "this.patch=(this.patch)+1"
fi




npm install
npm run build;
