<?php
require_once __DIR__ . '/vendor/autoload.php';
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('max_execution_time', 300);
ini_set("pcre.backtrack_limit", "1000000000");
ini_set('memory_limit', '-1');
ob_start();
$data = json_decode($_POST['dataquotation'],  true);
// print_r($data);
// echo $data['datenow_datepicker'];
$countdatenow = strlen($data['datenow_datepicker']);
// echo $countdatenow;

$delivery = $data['delivery_date'];
$date2 = explode('-', $delivery);
$year = $date2[0] + 543;
$month   = $date2[1];
$day  = $date2[2];
$delivery_date = $day . '/' . $month . '/' . $year;

$due = $data['due_date'];
$data3 = explode('-', $due);
$year = $data3[0] + 543;
$month   = $data3[1];
$day  = $data3[2];
$due_date = $day . '/' . $month . '/' . $year;

if ($countdatenow <= 13) {
  $convertdate = $data['datenow_datepicker'];
  $datenow_datepicker =  date("d/m/Y", $convertdate);
}
if ($countdatenow > 20) {
  $beforecut = $data['datenow_datepicker'];
  $cutstring = substr($beforecut, 0, 10);
  $data4 = explode('-', $cutstring);
  $year = $data4[0] + 543;
  $month   = $data4[1];
  $day  = $data4[2];
  $datenow_datepicker = $day . '/' . $month . '/' . $year;
}

?>

<html>

<head>
  <title>Print Quotation A5</title>
  <style scope>
    @import url('https://fonts.googleapis.com/css?family=Niramit');

    * {
      padding: 0;
      margin: 0;
      box-sizing: border-box;
    }

    @page {
      /* size: A5; */
      margin: 20px;
    }

    /* @media print {
            html,
            body {
                width: 297mm;
                height: 210mm;
            }
        } */

    html {
      margin: 0px;
      /* this affects the margin on the html before sending to printer */
    }

    body {
      /* display:block !important; */
      max-width: 714px;
      /* border: solid 1px blue;*/
      margin: 4mm 5mm 5mm 4mm;
      /* font-size: 30px; */
      /* margin you want for the content */
    }



    p {
      font-size: 15px;
      line-height: 12px;
      margin-left: 5px
        /* font-family: 'Niramit', sans-serif; */
    }

    .underline {
      border-bottom: solid 1px black;
      width: 250px;
      height: 1px;
    }

    svg {
      position: absolute !important;
      width: 209px !important;
      height: 69px !important;
      top: 17px !important;
      left: 165px !important;
    }
  </style>
</head>

<body>
  <div style="width: 100%;">
    <div style="width: 100%;max-height: 20%;">
      <div style="float: left; padding: 0px; width: 50%;">
        <img src="logo.jpg" style="width: 140px; height: 70px; position: static; left: 0px; top: 0px;">
        <div style="float: left; padding: 0px; width: 30%;">
          <!-- <svg id="code128"></svg> -->
        </div>
        <span style="position: static; left: 2.3rem; font-weight: 300; background: white; z-index: 99; top: 77px;">เลขประจำตัวผู้เสียภาษี 0505533000157</span>
      </div>
      <div style="float: right;text-align: right;position: relative">
        <div>
          <span style="font-weight: bold;">F-WS-301</span>
          <div style="position: absolute;float: right;" class="underline"></div>
        </div>
        <div style="text-align: right;margin-top: 12px">
          <span style="font-weight: 500;">ใบเสนอราคา/ใบรับคำสั่งซื้อ</span>
          <div style="position: absolute;float: right;" class="underline"></div>
        </div>
        <div style="margin-top: 6px">
          <span style="font-weight: 500;">RunNumber/Copy : <?php echo $data['doc_no']; ?>/1.00 <span style="font-weight: bold">QUOTATION</span> </span>
        </div>
      </div>
    </div>

    <div style="width: 100%; background-color: #f8f8f8;">
      <div style="float: left; padding: 0px; width: 49.8%; position: relative;border: 1px solid black;border-radius: 8px; height: 15%;">
        <div>
          <div style="width: 10%;float: left;">
            <p>ชื่อลูกค้า</p>
          </div>
          <div style="float: left; position: relative;width: 80%;">
            <p style="text-decoration:underline;"><?php echo $data['ar_code']; ?> <?php echo $data['ar_name']; ?></p>
          </div>
          <div style="width: 10%;float: left;">
            <p>ที่อยู่</p>
          </div>
          <div style=" float: left; position: relative;width: 90%;">
            <p style="position: relative;"><?php echo $data['ar_bill_address']; ?> โทร 081-6711966</p>
          </div>
          <div style="width: 10%;float: left;">
            <p>โทร</p>
          </div>
          <div style="float: left; position: relative;width: 90%;">
            <p style="position: relative;"> 081-6711966</p>
          </div>
          <div style="width: 20%;float: left;">
            <p>วันที่นัดส่งสินค้า </p>
          </div>
          <div style="float: left; position: relative;width: 80%;">
            <p><?php echo $delivery_date ?></p>
          </div>
          <div style="width: 5%;float: left;">
            <p>*</p>
          </div>
          <div style="float: left; position: relative;width: 95%;">
            <p>สินค้าสั่งพิเศษ ลูกค้าไม่รับตามกำหนด 30 วัน จะคิดค่าบริการคลัง 1% ของมูลค่าสินค้า</p>
          </div>
        </div>
      </div>

      <div style="float: right; padding: 0px; width: 50%;  height: 15%;">
        <div style="position: relative;border: 1px solid black;border-radius: 8px;width: 100%;">
          <div style="width: 20%;float: left;">
            <p>เลขที่ใบเสนอราคา</p>
          </div>
          <div style="float: left; position: relative;width: 78%;">
            <p><?php echo $data['doc_no']; ?></p>
          </div>
          <div style="width: 20%;float: left;">
            <p>วันที่ออกเอกสาร</p>
          </div>
          <div style="float: left; position: relative;width: 78%;">
            <p>asdasd<?php //echo $datenow_datepicker ?></p>
          </div>
          <div style="width: 100%;">
            <div style="float: left;text-align: left;width: 40%;">
              <p>เงื่อนไขการชำระเงิน</p>
            </div>
            <div style="float: left;width: 20%;position: relative;">
              <p style=""><?php echo $data['credit_day'] ?></p>
            </div>
            <div style="float: left;position: relative;">
              <p>วัน</p>
            </div>
          </div>
          <div style="width: 100%;">
            <div style="float: left;text-align: left; width: 40%;">
              <p>ยืนราคาถึงวันที่</p>
            </div>
            <div style="float: left;width: 55%;position: relative;">
              <p><?php echo $due_date ?></p>
            </div>
          </div>
        </div>
        <!-- <div style="width: 10%;float: left;">
            <p>โทร</p>
          </div>
          <div style="float: left; position: relative;width: 90%;">
            <p style="position: relative;"> 081-6711966</p>
          </div> -->
        <div style="float: right;border: 1px solid black;border-radius: 8px;width: 100%;text-align: center; height: 147px;">
            <div style="width: 100%;height: 35px;">
              <p style="position: relative;">พนักงานขาย <?php echo $data['sale_code'] ?> <?php echo $data['sale_name'] ?> , โทร - , มือถือ - , อีเมลล์ -</p>
            </div>
          <div style="width: 100%;text-align: center;color: grey">
            <div>
              <p> โทร: (053) 240-377 แฟกซ์: (053) 244-111</p>
            </div>
            <div style="position: relative;top: 3px;color: black">
              <p>ComputerName: NEWAPPSERVICE/LogINName : administrator</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div style="height: 18px;position: relative;top: 4px">
      <span style="float: left">ขอขอบพระคุณที่ท่านไว้วางใจในบริการเรา ทางบริษัทฯมีความยินดีที่จะเสนอราคาสินค้า ดังต่อไปนี้</span>
      <span style="float: right;">
        <!-- <button class="printnone" style="padding: 0 20px; position: relative; top: -7px; height: 19px;" onclick="javascript:window.print()"><span>Print</span></button> -->
        <!-- <select onchange="sizepage()" class="printnone" id="valuepage" style="position: relative; top: -8px;"><option value="615.77">Page 1</option>
        <option value="1765">Page 2</option><option value="2850">Page 3</option><option value="4000">Page 4</option></select></span> -->
    </div>

    <div id="heightproduct" style="width: 100%;border-top: 1px solid black;border-left: 1px solid black;height: 615.77px;border-right: 1px solid black;border-radius: 5px">
      <div style="width: 100%; height: 25px;border-bottom: 1px solid black; text-align: center">
        <div style=" float: left;width: 5%;border-right: 1px dashed grey;position: relative">
          <p style="position: absolute;bottom: 3px;left: 2px">ลำดับ</p>
        </div>
        <div style="float: left;width: 39%;border-right: 1px dashed grey;position: relative">
          <p style="position: absolute;bottom: 3px;left: 5px">รหัสสินค้า/รายละเอียด</p>
        </div>

        <div style="float: left;width: 13.75%;border-right: 1px dashed grey;position: relative">
          <p style="position: absolute;bottom: 3px;right: 5px">จำนวน</p>
        </div>
        <div style="float: left;width: 13.75%;border-right: 1px dashed grey;position: relative">
          <p style="position: absolute;bottom: 3px;right: 5px">หน่วย</p>
        </div>
        <div style="float: left;width: 13.75%;border-right: 1px dashed grey;position: relative">
          <p style="position: absolute;bottom: 3px;right: 5px">ราคาต่อหน่วย</p>
        </div>
        <div style="float: left;width: 13.75%;position: relative">
          <p style="position: absolute;bottom: 3px;right: 5px">ราคารวมภาษี</p>
        </div>
      </div>

      <div id="counttable" style="width: 100%;height: 57%;border-bottom: 1px solid black;position: relative">
        <?php
        $count = count($data['subs']);
        $detail = $data['subs'];
        for ($i = 0; $i < $count; $i++) {
          ?>
          <div style="position: relative;min-height: 20px;width: 100%;float: left">
            <div style="width: 5%;float:left;position: relative;line-height: 20px;text-align: center; border-right: 1px dashed grey">
              <p style=""><?php $line = $i + 1;
                            echo $line; ?></p>
            </div>
            <div style="width: 38.75%;float:left;position: relative;line-height: 20px;padding-left: 5px; border-right: 1px dashed grey">
              <p style=""><?php echo $detail[$i]['item_code']; ?> <?php echo $detail[$i]['item_name']; ?></p>
            </div>
            <div style="width: 13.75%;float:left;position: relative;text-align: right;line-height: 20px; border-right: 1px dashed grey">
              <p style="margin-right: 5px"><?php echo number_format((float) $detail[$i]['qty'], 2, '.', '') ?></p>
            </div>
            <div style="width: 13.75%;float:left;position: relative;text-align: right;line-height: 20px; border-right: 1px dashed grey">
              <p style="margin-right: 5px"><?php echo $detail[$i]['unit_code']; ?></p>
            </div>
            <div style="width: 13.75%;float:left;position: relative;text-align: right;line-height: 20px; border-right: 1px dashed grey">
              <p style="margin-right: 5px"><?php echo number_format((float) $detail[$i]['price'], 2, '.', '') ?></p>
            </div>
            <div style="width: 13.75%;float:left;position: relative;text-align: right;line-height: 20px;">
              <p style="margin-right: 5px"><?php echo number_format((float) $detail[$i]['item_amount'], 2, '.', '') ?></p>
            </div>
          </div>
        <?php  } ?>
      </div>
    </div>

    <div style="height: 185px;border: 1px solid black;border-radius: 5px">
      <div style="float:left;position: relative;padding-top: 5px;padding-left: 5px;width: 35%;height: 100%;border-right: 1px dashed grey;">
        <p> หมายเหตุ</p>
        <div style="position:absolute;bottom: -10px">
          <div style="margin-bottom: 8px">
            <p style="font-weight: bold"> * ไม่ใช่ใบเสร็จรับเงิน *</p>
          </div>
          <div style="margin-bottom: 8px">
            <p style="font-weight: bold"> * กรุณาเรียกใบเสร็จทุกครั้งที่ชำระเงิน *</p>
          </div>
          <div style="margin-bottom: 8px">
            <p style="font-weight: bold"> * มิฉะนั้น ทางบริษัทฯไม่รับผิดชอบ *</p>
          </div>
        </div>
      </div>

      <div style="display:block;">
        <div style="width:30%;float: left;border-bottom: 1px solid black;border-right: 1px dashed grey;text-align:center;height: 150px">
          <div style="width: 100%;position: relative">
            <p style="position: absolute;bottom: 0;white-space: nowrap"> <?php echo $data['sale_code'] ?> <?php echo $data['sale_name'] ?> </p>
          </div>
          <div style="width: 100%;position: relative">
            <p style="position: absolute;bottom: 5px;"> ผู้ทำรายการ</p>
          </div>
        </div>
        <div style="width:25%;float: left;border-bottom: 1px solid black;border-right: 1px dashed grey;position: relative;height: 150px">
          <div style="padding-left: 5px;width: 10%;text-align: left;float: left;">
            <p style="text-decoration: underline;">หัก</p>
          </div>
          <div style="padding-left: 5px;width: 35%;text-align: left;float: left;">
            <p>
              <?php
              if ($data['discount_select'] == TRUE) {
                echo "ส่วนลด รวม";
              }
              ?>
            </p>
          </div>
          <p style="position: relative;float: left;">
            <?php
            if ($data['discount_select'] == TRUE) {
              echo number_format((float) $data['discount_amount'], 2, '.', '');
            } ?>
          </p>
        </div>
        <div style="width:44%;float: left;border-bottom: 1px solid black;position: relative;height: 150px">
          <div style="width: 100%;position: relative">
            <div style="width: 50%;text-align: right;position: absolute;bottom: 0;float: left;">
              <p style="position: relative;"> ราคาสินค้า</p>
            </div>
            <div style="width: 50%;text-align: right;position: absolute;bottom: 0;left: 50%">
              <p style="position: relative;right: 5px"> <?php echo number_format((float) $data['after_discount_amount'], 2, '.', '') ?> </p>
            </div>
          </div>
          <div style="width: 100%;position: relative">
            <div style="width: 50%;text-align: right;position: absolute;bottom: 0;float: left;">
              <p style="position: relative;"> จำนวนภาษีมูลค่าเพิ่ม</p>
            </div>
            <div style="width: 50%;text-align: right;position: absolute;bottom: 0;left: 50%">
              <p style="position: relative;right: 5px"> <?php echo number_format((float) $data['dif_fee'], 2, '.', '') ?> </p>
            </div>
          </div>
          <div style="width: 100%;position: relative">
            <div style="width: 50%;text-align: right;position: absolute;bottom: 0;float: left;">
              <p style="position: relative;">จำนวนเงินทั้งสิ้น</p>
            </div>
            <div style="width: 50%;text-align: right;position: absolute;bottom: 0;left: 50%">
              <p style="position: relative;right: 5px"> <?php echo number_format((float) $data['after_discount_amount'], 2, '.', '') ?> </p>
            </div>
          </div>
        </div>
      </div>

      <div style="display:block">
        <div style="width: 100%;float: left;border-bottom: 1px dashed grey;position: relative;">
          <p style="font-weight: bold;position: absolute;left:10px">ตัวอักษร: <p id="thaimoney"></p>
          </p>
        </div>
      </div>

      <div style="display:block;">
        <div style="width: 100%;float: left;position: relative;height: 200px">
          <div style="width: 28%;border-right: 1px dashed grey;float: left;height: 200px">
            <div style="width: 100%;text-align: center">
              <p style="position: relative;top: 10px"> <?php echo $data['sale_code'] ?> <?php echo $data['sale_name'] ?></p>
            </div>
            <div style="width: 100%;text-align: center;position: relative">
              <div>
                <p style="position:relative;top: 15px">............/............/............</p>
              </div>
              <div>
                <p style="position: relative;top: 20px;">พนักงานขายผู้เสนอราคา</p>
              </div>
            </div>
          </div>

          <div style="width: 28%;border-right: 1px dashed grey;float: left;height: 200px">
            <div style="width: 100%;text-align: center">
              <p style="position: relative;top: 10px">ขอแสดงความนับถือ</p>
            </div>
            <div style="width: 100%;text-align: center;position: relative">
              <div>
                <p style="position:relative;top: 15px">............/............/............</p>
              </div>
              <div>
                <p style="position: relative;top: 20px;">ผู้อนุมัติการเสนอราคา</p>
              </div>
            </div>
          </div>

          <div style="width: 43%;float: left">
            <div style="width: 100%;text-align: center">
              <p style="position: relative;top: 10px">อนุมัติสั่งซื้อตามใบเสนอราคานี้</p>
            </div>
            <div style="width: 100%;text-align: center;position: relative">
              <div>
                <p style="position:relative;top: 15px">............/............/............</p>
              </div>
              <div>
                <p style="position: relative;top: 20px;">ลงชื่อผู้มีอำนาจในการสั่งซื้อ พร้อมประทับตรา</p>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="convertthaimoney.js"></script>
  <script src="JsBarcode.all.min.js"></script>

  <script>
    // alert($("#counttable div").length)
    // alert('dasdsa')
    var heightproduct = document.getElementById('heightproduct').clientHeight;
    var valuepage = document.getElementById('valuepage').value

    // alert(valuepage)
    // alert(document.getElementById('heightproduct').clientHeight)
    // document.getElementById('heightproduct').style.height = '1765'
    var totalmoney = document.getElementById('convertmoney').value;
    console.log(totalmoney);
    var inputthaimoney = document.getElementById('thaimoney');
    var thaimoney = ArabicNumberToText(totalmoney.toString());

    inputthaimoney.innerHTML = thaimoney;

    var docno = document.getElementById('barcodedocno').value
    JsBarcode("#code128", docno);

    //   window.print()
    function sizepage() {
      // alert('dasda')
      document.getElementById('heightproduct').style.height = document.getElementById('valuepage').value
    }
  </script>
</body>

</html>

<?php
// date_default_timezone_set("Asia/Bangkok");
// $datetime = date("d/m/Y H:i:s");
// $footer = "<div style='width:100%; text-align:right;' lang=\"th\">บันทึก ณ วันที่ ".$datetime."</div>";

$html = ob_get_contents();
ob_end_clean();
$pdf = new \Mpdf\Mpdf(['mode' => 'th', 'format' => [420, 595]]);
$pdf->WriteHTML($html);
// $pdf->Setfooter($footer);
$pdf->Output('report.pdf', \Mpdf\Output\Destination::INLINE);
?>
