import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const state = {
  kpi: {
    Branch: "",
    SaleCode: 0,
    Department: "",
    Datetime: "",
    TPPoint: 0,
    Assessor: []
  }
};

const store = new Vuex.Store({
  state
});
export default store;
