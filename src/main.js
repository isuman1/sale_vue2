import Vue from "vue";
import App from "./App";
import router from "./router";
import VueRangedatePicker from 'vue-rangedate-picker'
import VueMaterial from "vue-material";
import { MdButton, MdContent, MdTabs } from "vue-material/dist/components";
import "vue-material/dist/vue-material.min.css";
import "vue-material/dist/theme/default.css"; // This line here
import "jspdf-customfonts/dist/jspdf.customfonts.min.js";
import VueHotkey from "v-hotkey";
import "vuetify/dist/vuetify.min.css";
import VueApexCharts from "vue-apexcharts";
import vSelect from "vue-select";
import VueMoment from "vue-moment";
import moment from "moment-timezone";
import GSignInButton from "vue-google-signin-button";
import money from "v-money";
import autofocus from "vue-autofocus-directive";
import Vuesax from "vuesax";
import store from "./Vuex/store";
import VueSweetalert2 from "vue-sweetalert2";
import vuetify from "./plugins/vuetify";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
Vue.use(vuetify);
Vue.use(Vuetify);
Vue.use(VueSweetalert2);
import "vuesax/dist/vuesax.css"; //Vuesax styles
Vue.use(Vuesax);
Vue.use(GSignInButton);
Vue.use(VueMoment, {
  moment
});
import Antd from "ant-design-vue";

import "ant-design-vue/dist/antd.css";
Vue.config.productionTip = false;


Vue.use(VueRangedatePicker)
Vue.use(Antd);
// Register components in your 'main.js'

Vue.use(VueApexCharts);
Vue.use(VueHotkey);
Vue.use(MdButton);
Vue.use(MdContent);
Vue.use(MdTabs);
Vue.use(VueMaterial);
Vue.config.productionTip = false;
Vue.component("v-select", vSelect);
Vue.use(money, { precision: 4 });
Vue.directive("autofocus", autofocus);

// /* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  store,
  vuetify,
  components: { App },
  template: "<App/>"
});
// new Vue({
//   router,
//   store,
//   vuetify,
//   render: h => h(App)
// }).$mount("#app");
