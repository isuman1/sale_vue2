import Vue from "vue";
import Router from "vue-router";
import index from "@/components/index";
import login from "@/components/login";
import quotation from "@/components/quotation/quotation";
import sale from "@/components/quotation/quotation";
import settinglist from "@/components/setting/settinglist";
//import setting from '@/components/setting/setting'
import setting from "@/components/setting/setting";
import newquo from "@/components/quotation/newquo";
import newsale from "@/components/sale/newsale";
import depositlist from "@/components/deposit/depositlist";
import saleorder from "@/components/saleorder/saleorder";
import newsaleorder from "@/components/saleorder/newsaleorder";
import dashboard from "@/components/dashboard/dashboard";
import utility from "@/components/utility/utility";
import dp from "@/components/deposit/deposit";
import sms from "@/components/mk/sms";
import quolist from "@/components/quotation/quotationlist";
import solist from "@/components/saleorder/saleorderlist";
import invoice from "@/components/invoice/invoice";
import invoicelist from "@/components/invoice/invoicelist";
import salehistory from "@/components/saleorder/salehistory";
import salehistorydetail from "@/components/saleorder/salehistorydetail";
import promptpay from "@/components/promptpay/promptpay";
import depositDev from "@/components/deposit/depositDev";
import addcustomer from "@/components/customer/addcustomer";
import kpi from "@/components/kpi/Kpi";
import Full from "@/contaniner/Full.vue";
import auth from "../service/auth.js";
import customerlist from "@/components/customer/customerlist";
import kpirank from "@/components/kpi/kpirank.vue";
import kpilist from "@/components/kpi/listKpi.vue";
import reportposlist from "@/components/report/reportinvoice/reportlist.vue";
import poslist from "@/components/report/pos/poslist.vue";
Vue.use(Router);
function requireAuth(to, from, next) {
  if (!auth.loggedIn()) {
    next({
      path: "/",
      query: { redirect: to.fullPath }
    });
  } else {
    next();
  }
}
export default new Router({
  routes: [
    // {
    //   path: '/index',
    //   name: 'index',
    //   component: index
    // },
    {
      path: "/index",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "index",
          component: dashboard
        }
      ]
    },
    {
      path: "/kpi",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "kpi",
          component: kpi
        }
      ]
    },
    {
      path: "/kpilist",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "kpilist",
          component: kpilist
        }
      ]
    },
    {
      path: "/kpirank",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "kpirank",
          component: kpirank
        }
      ]
    },
    {
      path: "/",
      redirect: "file",
      name: "file",
      component: {
        render(c) {
          return c("router-view");
        }
      },
      children: [
        {
          path: "",
          name: "login",
          component: login
        }
      ]
    },
    {
      path: "/invoicelist",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "invoicelist",
          component: invoicelist
        }
      ]
    },
    {
      path: "/invoice/:id",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "invoice",
          component: invoice
        }
      ]
    },
    {
      path: "/quotation/:id",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "quotation",
          component: quotation
        }
      ]
    },
    {
      path: "/settinglist",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "settinglist",
          component: settinglist
        }
      ]
    },
    {
      path: "/setting/:id",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "setting",
          component: setting
        }
      ]
    },
    {
      path: "/sale/:id",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "sale",
          component: sale
        }
      ]
    },
    {
      path: "/salehistory",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "salehistory",
          component: salehistory
        }
      ]
    },
    {
      path: "/salehistorydetail/:id",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "salehistorydetail",
          component: salehistorydetail
        }
      ]
    },

    {
      path: "/poslist",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "poslist",
          component: poslist
        }
      ]
    },
    {
      path: "/posdetail/:name",
      redirect: "file",
      name: "file",
      component: Full,
      props: true,
      children: [
        {
          path: "",
          name: "posdetail",
          component: poslist
        }
      ]
    },
    {
      path: "/reportposlist",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "reportposlist",
          component: reportposlist
        }
      ]
    },
    {
      path: "/saleorder/:id",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "saleorder",
          component: saleorder
        }
      ],
      props: {
        payload: Object
      }
    },
    {
      path: "/newquotation/:id",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "newquo",
          component: newquo
        }
      ]
    },
    {
      path: "/newsale",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "newsale",
          component: newsale
        }
      ]
    },
    {
      path: "/depositlist",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "depositlist",
          component: depositlist
        }
      ]
    },
    {
      path: "/newsaleorder/:id",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "newsaleorder",
          component: newsaleorder
        }
      ]
    },
    {
      path: "/utility",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "utility",
          component: utility
        }
      ]
    },
    {
      path: "/dp/:id",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "dp",
          component: dp
        }
      ]
    },
    {
      path: "/depositDev/:id",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "depositDev",
          component: depositDev
        }
      ]
    },
    {
      path: "/sms",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "sms",
          component: sms
        }
      ]
    },
    {
      path: "/quolist",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "quolist",
          component: quolist
        }
      ]
    },
    {
      path: "/solist",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "solist",
          component: solist
        }
      ]
    },
    {
      path: "/promptpay",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "promptpay",
          component: promptpay
        }
      ]
    },
    {
      path: "/addcustomer/:id",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "addcustomer",
          component: addcustomer
        }
      ]
    },
    {
      path: "/customerlist",
      redirect: "file",
      name: "file",
      component: Full,
      children: [
        {
          path: "",
          name: "customerlist",
          component: customerlist
        }
      ]
    }
    // },
    // {
    //   path: "/index",
    //   name: "index",
    //   component: dashboard
    // },
    // {
    //   path: "/",
    //   name: "login",
    //   component: login
    // },
    // {
    //   path: "/invoicelist",
    //   name: "invoicelist",
    //   component: invoicelist
    // },
    // {
    //   path: "/invoice/:id",
    //   name: "invoice",
    //   component: invoice
    // },
    // {
    //   path: "/quotation/:id",
    //   name: "quotation",
    //   component: quotation
    // },
    // {
    //   path: "/settinglist",
    //   name: "settinglist",
    //   component: settinglist
    // },
    // {
    //   path: "/setting/:id",
    //   name: "setting",
    //   component: setting
    // },
    // /*{
    //   path: '/setting',
    //   name: 'setting',
    //   component: setting
    // },*/
    // {
    //   path: "/sale/:id",
    //   name: "sale",
    //   component: sale
    // },
    // {
    //   path: "/salehistory",
    //   name: "salehistory",
    //   component: salehistory
    // }, //salehistory
    // {
    //   path: "/salehistorydetail/:id",
    //   name: "salehistorydetail",
    //   component: salehistorydetail
    // },
    // {
    //   path: "/saleorder/:id",
    //   name: "saleorder",
    //   component: saleorder
    // },
    // {
    //   path: "/saleorder/:id",
    //   name: "saleorder2",
    //   component: saleorder,
    //   props: {
    //     payload: Object
    //   }
    // },
    // {
    //   path: "/newquotation/:id",
    //   name: "newquo",
    //   component: newquo
    // },
    // {
    //   path: "/newsale/:id",
    //   name: "newsale",
    //   component: newsale
    // },
    // {
    //   path: "/depositlist",
    //   name: "depositlist",
    //   component: depositlist
    // },
    // {
    //   path: "/newsaleorder/:id",
    //   name: "newsaleorder",
    //   component: newsaleorder
    // },

    // {
    //   path: "/utility",
    //   name: "utility",
    //   component: utility
    // },
    // {
    //   path: "/dp/:id",
    //   name: "dp",
    //   component: dp
    // },
    // {
    //   path: "/depositDev/:id",
    //   name: "depositDev",
    //   component: depositDev
    // },
    // {
    //   path: "/sms",
    //   name: "sms",
    //   component: sms
    // },
    // {
    //   path: "/quolist",
    //   name: "quolist",
    //   component: quolist
    // },
    // {
    //   path: "/solist",
    //   name: "solist",
    //   component: solist
    // },
    // {
    //   path: "/promptpay",
    //   name: "promptpay",
    //   component: promptpay
    // },
    // {
    //   path: "/addcustomer",
    //   name: "addcustomer",
    //   component: addcustomer
    // }
  ]
});
