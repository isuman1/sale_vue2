import api from "./service.js";
export default {
  login(id, password, cb) {
    cb = arguments[arguments.length - 1];
    if (sessionStorage.token) {
      if (cb) cb(true);
      this.onChange(true);
      return;
    }
    pretendRequest(id, password, res => {
      if (res.authenticated) {
        sessionStorage.token = res.token;

        if (cb) cb(true);
        this.onChange(true);
      } else {
        if (cb) cb(false);
        this.onChange(false);
      }
    });
  },

  getToken() {
    return sessionStorage.token;
  },

  logout(cb) {
    delete sessionStorage.token;
    delete sessionStorage.userid;
    delete sessionStorage.company_id;
    delete sessionStorage.Datauser;
    delete sessionStorage.branch_id;
    delete sessionStorage.profit_code;
    delete sessionStorage.own_team;
    delete sessionStorage.parent_team;
    delete sessionStorage.setting;
    if (cb) cb();
    this.onChange(false);
  },

  loggedIn() {
    return !!sessionStorage.token;
  },

  onChange() {}
};
function pretendRequest(id, password, cb) {
  setTimeout(() => {
    api.signin(
      id,
      password,
      result => {
        if (result.status == "invalid connection") {
          alertify.error("เกิดข้อผิดพลาด");
          return;
        }
        if (result.status === "success") {
          console.log(result.data);
          cb({
            authenticated: true,
            token: Math.random()
              .toString(36)
              .substring(7)
          });
          sessionStorage.Datauser = JSON.stringify(result.data);
          alertify.success("Login Success");
          sessionStorage.userid = result.data.id;
          sessionStorage.company_id = result.data.company_id;
          sessionStorage.branch_id = result.data.branch_id;
          sessionStorage.own_team = result.data.own_team;
          sessionStorage.profit_code = result.data.profit_code;
          sessionStorage.parent_team = result.data.parent_team;
        } else {
          cb({ authenticated: false });
          alertify.error("เกิดข้อผิดพลาด");
        }
      },
      error => {
        setTimeout(() => {
          switch (error.response.data.message) {
            case "No Content = UserName Not Active":
              alertify.error("ผู้ใช้ถูกระงับการใช้งาน");
              break;
            case "No Content = UserName or Password Invalid":
              alertify.error("ผู้ใช้ไม่มีสิทธิในแอพนี้หรือรหัสผ่านไม่ถูกต้อง");
              break;
            default:
              alertify.error("เกิดข้อผิดพลาดนะ");
              this.loading = false;
              break;
          }
        }, 10);
        cb({ authenticated: false });
      }
    );
  }, 0);
}
