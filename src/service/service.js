import Vue from "vue";
import axios from "axios";
import Vueaxios from "vue-axios";
import vs from "./version.json";
Vue.use(Vueaxios, axios);
axios.defaults.headers.post["Content-Type"] = "application/json";
//const URL = "http://localhost:9999/";
//const URL = vs.apibase;
const URL = "https://dev.nopadol.com/";
const URLVnus = "http://192.168.0.70:8079/";
const npsysURL = "https://sys.nopadol.com/";
const telURL = "https://sheetdb.io";
const smsURL = "https://api.apitel.co/sms";
const test = "http://192.168.0.83:8080/v2/atm/auth";

export default {
  signin(user, pass, success, error) {
    Vue.axios
      .get(
        npsysURL +
          "login?access_token=aaaa&usercode=" +
          user +
          "&password=" +
          pass +
          "&appid=1"
      )
      .then(
        response => {
          console.log(response.data);
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  FindKpilist(payload, success, error) {
    Vue.axios.post(URL + "kpi/v1/list/assessor", payload).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  CheckStatusVenus(success, error) {
    Vue.axios.get(URLVnus + "sync/v1/status").then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  FindReportSalePos9(payload, success, error) {
    Vue.axios.post(URL + "report/v1/find/report/salebydate", payload).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },

  FindListPos9(payload, success, error) {
    Vue.axios.post(URL + "report/v1/list/pos", payload).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  FindReportItemSalePos9(payload, success, error) {
    Vue.axios.post(URL + "report/v1/find/report/itemsalebydate", payload).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  searchDocByUser(payload, success, error) {
    Vue.axios.post(URL + "sales/v1/sale/doc/search/user", payload).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  showdocall(payload, success, error) {
    Vue.axios.post(URL + "sales/v1/sale/doc/search", payload).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  Customerall(payload, success, error) {
    payload.keyword = payload.keyword.replace(" ", "%");
    Vue.axios.post(URL + "customer/v1/search/keyword", payload).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  CustomerNew(payload, success, error) {
    Vue.axios.post(URL + "customer/v1/new", payload).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  CustomerUpdate(payload, success, error) {
    Vue.axios.post(URL + "customer/v1/update", payload).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  findCustomerID(payload, success, error) {
    Vue.axios.post(URL + "customer/v1/search/id", payload).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  showdocno(payload, success, error) {
    console.log(JSON.stringify(payload));
    Vue.axios.post(URL + "gendocno/v1/gen", JSON.stringify(payload)).then(
      response => {
        console.log(response.data);
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  cancelinvoice(payload, success, error) {
    console.log(JSON.stringify(payload));

    Vue.axios.post(URL + "sales/v1/inv/cancel", JSON.stringify(payload)).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  searchbykeyword(payload, success, error) {
    console.log(JSON.stringify(payload.keyword));
    Vue.axios
      .post(URL + "product/v1/search/keyword", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchcus(payload, success, error) {
    Vue.axios
      .post(URL + "employee/v1/search/keyword", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  saveInvoice(payload, success, error) {
    Vue.axios.post(URL + "sales/v1/inv/new", JSON.stringify(payload)).then(
      response => {
        console.log(JSON.stringify(response.data));
        console.log("บันทึก");
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  checkpermission(index) {
    console.log("tag", sessionStorage.Datauser);
    let sess = JSON.parse(sessionStorage.Datauser);
    if (sess.menu[index].is_read == 1) {
      return true;
    }
    return false;
  },
  savequotation(payload, success, error) {
    Vue.axios.post(URL + "sales/v1/quo/new", JSON.stringify(payload)).then(
      response => {
        console.log(JSON.stringify(response.data));
        console.log("บันทึก");
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  searchinvoicelist(payload, success, error) {
    Vue.axios.post(URL + "sales/v1/inv/list", payload).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  searchcreditcard(payload, success, error) {
    console.log(payload);
    Vue.axios
      .post(URL + "sales/v1/search/creditcard", JSON.stringify(payload))
      .then(
        response => {
          console.log(JSON.stringify(response.data));
          console.log("บันทึก");
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  detailquoall(payload, success, error) {
    Vue.axios
      .post(URL + "sales/v1/quo/search/id", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  detailsaleall(payload, success, error) {
    Vue.axios
      .post(URL + "sales/v1/sale/search/id", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchQuotationByKeyword(payload, success, error) {
    Vue.axios
      .post(URL + "sales/v1/quo/search/keyword", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchSaleOrderByKeyword(payload, success, error) {
    Vue.axios
      .post(URL + "sales/v1/sale/search/keyword", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  detailinvall(payload, success, error) {
    Vue.axios
      .post(URL + "sales/v1/inv/search/id", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchdepartment(payload, success, error) {
    payload.keyword = payload.keyword.replace(" ", "%");
    Vue.axios
      .post(URL + "env/v1/department/search/keyword", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchdepartmentById(payload, success, error) {
    Vue.axios
      .post(URL + "env/v1/department/search/id", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchproject(payload, success, error) {
    payload.keyword = payload.keyword.replace(" ", "%");
    Vue.axios
      .post(URL + "env/v1/project/search/keyword", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchprojectById(payload, success, error) {
    Vue.axios
      .post(URL + "env/v1/project/search/id", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchAllocate(payload, success, error) {
    payload.keyword = payload.keyword.replace(" ", "%");
    Vue.axios
      .post(URL + "env/v1/allocate/search/keyword", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchAllocateById(payload, success, error) {
    Vue.axios
      .post(URL + "env/v1/allocate/search/id", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchCustContactById(payload, success, error) {
    Vue.axios
      .post(URL + "env/v1/find/custcontact/id", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchCustContactByKeyword(payload, success, error) {
    Vue.axios
      .post(URL + "env/v1/find/custcontact/keyword", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchunitcode(payload, success, error) {
    Vue.axios
      .post(URL + "product/v1/search/itemcode", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  findDepartment(success, error) {
    Vue.axios.get(URL + "employee/v1/deapartment/search").then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  callsetting(payload, success, error) {
    Vue.axios
      .post(
        URL + "settingconfig/v1/config/get/configurationv2bybranch",
        payload
      )
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  callUnitCodebyCode(payload, success, error) {
    Vue.axios
      .post(URL + "sales/v1/find/unitcodebycode", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  callUnitCodebyCodeNoType(payload, success, error) {
    Vue.axios
      .post(URL + "sales/v1/find/unitcodebycodenotype", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  findSaleCodeDepartment(payload, success, error) {
    Vue.axios
      .post(
        URL + "employee/v1/sale/search/bydeapartment",
        JSON.stringify(payload)
      )
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  findDepartmentSaleCode(payload, success, error) {
    Vue.axios
      .post(
        URL + "employee/v1/sale/search/deapartmentbycode",
        JSON.stringify(payload)
      )
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  findEmployeeCode(success, error) {
    Vue.axios.get(URL + "employee/v1/sale/search/bycode").then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  CreateKpiRanking(payload, success, error) {
    Vue.axios
      .post(
        URL + "employee/v1/hr/kpi/createkpirankscore",
        JSON.stringify(payload)
      )
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  findDepartmentV2(payload, success, error) {
    Vue.axios
      .post(
        URL + "employee/v1/sale/find/deapartment/withbranch",
        JSON.stringify(payload)
      )
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  findSaleCodeDepartmentV2(payload, success, error) {
    Vue.axios
      .post(URL + "employee/v1/sale/find/salecode", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  CreateKpiRankingV2(payload, success, error) {
    Vue.axios
      .post(URL + "employee/v1/sale/create/kpi", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },

  createdeposit(payload, success, error) {
    Vue.axios.post(URL + "sales/v1/dep/new", JSON.stringify(payload)).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  searchDepById(payload, success, error) {
    Vue.axios
      .post(URL + "sales/v1/dep/search/id", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchDepByKeyword(payload, success, error) {
    payload.keyword = payload.keyword.replace(" ", "%");
    Vue.axios
      .post(URL + "sales/v1/dep/search/keyword", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchInvById(payload, success, error) {
    Vue.axios
      .post(URL + "sales/v1/inv/search/id", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchInvByKeyword(payload, success, error) {
    payload.keyword = payload.keyword.replace(" ", "%");
    Vue.axios
      .post(URL + "sales/v1/inv/search/keyword", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  SearchHisByKeyword(payload, success, error) {
    payload.keyword = payload.keyword.replace(" ", "%");
    Vue.axios
      .post(URL + "sales/v1/his/search/keyword", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },

  SearchsoBypofitown(payload, success, error) {
    Vue.axios
      .post(URL + "sales/v1/sale/so/searchbyprofitown", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  SearchQtBypofitown(payload, success, error) {
    Vue.axios
      .post(URL + "sales/v1/sale/qt/searchbyprofitown", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  SearchInvBypofitown(payload, success, error) {
    Vue.axios
      .post(
        URL + "sales/v1/sale/inv/searchbyprofitown",
        JSON.stringify(payload)
      )
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  SearchDepBypofitown(payload, success, error) {
    Vue.axios
      .post(
        URL + "sales/v1/sale/dep/searchbyprofitown",
        JSON.stringify(payload)
      )
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchSaleByItem(payload, success, error) {
    Vue.axios
      .post(URL + "sales/v1/sale/search/item", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchHisCustomer(payload, success, error) {
    Vue.axios
      .post(URL + "sales/v1/search/hiscustomer", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  showSetting(payload, success, error) {
    Vue.axios
      .post(
        URL + "settingconfig/v1/config/get/configurationv2",
        JSON.stringify(payload)
      )
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  createSetting(payload, success, error) {
    Vue.axios
      .post(URL + "settingconfig/v1/config/new", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchSetByKeyword(payload, success, error) {
    payload.keyword = payload.keyword.replace(" ", "%");
    Vue.axios
      .post(
        URL + "settingconfig/v1/config/search/keyword",
        JSON.stringify(payload)
      )
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  findproductByKeyword(payload, success, error) {
    Vue.axios
      .post(URL + "sales/v1/find/product/keyword", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchNote(payload, success, error) {
    Vue.axios.post(URL + "settingconfig/v1/note", JSON.stringify(payload)).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  department(payload, success, error) {
    Vue.axios
      .post(URL + "sales/v1/find/department", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchReserveByKeyword(payload, success, error) {
    payload.keyword = payload.keyword.replace(" ", "%");
    Vue.axios
      .post(URL + "sales/v1/dep/reserve/search", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  createsale(payload, success, error) {
    Vue.axios.post(URL + "sales/v1/sale/new", JSON.stringify(payload)).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  confirmQuotation(payload, success, error) {
    Vue.axios.post(URL + "sales/v1/quo/confirm", JSON.stringify(payload)).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  cancelQuotation(payload, success, error) {
    Vue.axios.post(URL + "sales/v1/quo/cancel", JSON.stringify(payload)).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  transferQTtoSO(payload, success, error) {
    Vue.axios
      .post(URL + "sales/v1/quo/gen/saleorder", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  findbanknp(success, error) {
    Vue.axios.get(URL + "sales/v1/find/banknp").then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  findbankbooknp(success, error) {
    Vue.axios.get(URL + "sales/v1/find/bankbooknp").then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  findbankbranch(success, error) {
    Vue.axios.get(URL + "sales/v1/find/bankbranch").then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  callTel(success, error) {
    Vue.axios.get(telURL + "/api/v1/x6r5jxmu9otla").then(
      response => {
        console.log(response.data);
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  testCallTel(success, error) {
    Vue.axios.get(telURL + "/api/v1/e9cklwsj2nqwr").then(
      response => {
        console.log(response.data);
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  sendSMS(payload, success, error) {
    Vue.axios.post(smsURL, payload).then(
      response => {
        console.log(response.data);
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  tester(payload, success, error) {
    Vue.axios.post(test, payload).then(
      response => {
        console.log(response.data);
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  callqrcode(data, success, error) {
    console.log("ส่ง JSON payment" + JSON.stringify(data));
    Vue.axios
      .post(
        "https://test.paybox.work/v1/payment/qrpay/open",
        JSON.stringify(data),
        {
          headers: {
            "Content-Type": "application/json"
            //'x-access-token': '246aa13b23f64f67be1ab463de0dcb72',
          }
        }
      )
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  SearchAddressById(payload, success, error) {
    Vue.axios
      .post(URL + "env/v1/find/delivery/id", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  SearchAddressByKeyword(payload, success, error) {
    Vue.axios
      .post(URL + "env/v1/find/delivery/keyword", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  createAddress(payload, success, error) {
    Vue.axios.post(URL + "env/v1/delivery/new", JSON.stringify(payload)).then(
      response => {
        success(response.data);
      },
      response => {
        error(response);
      }
    );
  },
  addCustContact(payload, success, error) {
    Vue.axios
      .post(URL + "customer/v1/custcontact/new", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchCompanyDetail(payload, success, error) {
    Vue.axios
      .post(URL + "settingconfig/v1/company/detail/id", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  },
  searchBranchDetail(payload, success, error) {
    Vue.axios
      .post(URL + "settingconfig/v1/branch/detail/id", JSON.stringify(payload))
      .then(
        response => {
          success(response.data);
        },
        response => {
          error(response);
        }
      );
  }
};
