const toLower = text => {
  return text.toString().toLowerCase();
};

const searchByName = (items, term) => {
  if (term) {
    return items.filter(
      item =>
        toLower(item.item_name).includes(toLower(term)) ||
        toLower(item.unit_code).includes(toLower(term))
    );
  }
  return items;
};
import Vue from "vue";
import Loading from "vue-loading-overlay";
import "vue-loading-overlay/dist/vue-loading.css";
import Datepicker from "vuejs-datepicker";
import Swal from "sweetalert2";
import * as lang from "vuejs-datepicker/src/locale";
import api from "../service/service.js";
import itemtable from "@/components/ui/tableitem";
import searchhiscustomer from "@/components/ui/searchhiscustomer";
import componentnote from "@/components/ui/componentnote";
import JQuery from "jquery";
import searchItem from "@/components/ui/searchItem";
let $ = JQuery;

export default {
  name: "saleorder",
  components: {
    Datepicker,
    itemtable,
    searchhiscustomer,
    componentnote,
    searchItem,
    Loading
  },
  data: () => ({
    job_id: "",
    ref_no: "",
    itemtable: [],
    searchhiscustomer: [],
    infoNotice: "",
    msg: "",
    typepage: "saleorder",
    selectedDate: null,
    date: "",
    search: [],
    search: "",
    objuser: JSON.parse(sessionStorage.Datauser),
    dproducts: [],
    active: "first",
    first: false,
    second: false,
    third: false,
    secondStepError: null,
    language: "th",
    languages: lang,
    idcus: "",
    searchcus: "",
    detailcus: "",
    showDialogcus: false,
    detailcusall: [],
    tablecode: "SO",
    billtype: "0",
    taxtype: 1,
    docno: "ไม่มีข้อมูล",
    mockdocno: "",
    keywordproduct: "",
    showDialogproduct: false,
    confirmDialog: false,
    selectQTdialog: false,
    selectCustomer: false,
    selectSale: false,
    dataproductDialog: [],
    disablebilltype: false,
    datenow_datepicker: Date.now(),
    follow_date: Date.now(),
    attention: "",
    percal: false, //true == % , false == บาท
    caldiscount: 0,
    keywordemp: "",
    discountSelect: true,
    customerLevel: "",
    isConfirm: 0,
    isCancel: 0,
    // page 2
    bill_credit: "",
    DueDate_cal: "",
    Deliver_date: 1,
    DueDate_date: "",
    expire_date: 1,
    expiredate_cal: "",
    isshowdocument: false,
    docheight: "72px",
    showAddrass: false,
    addressHeight: "72px",
    ass_id: 0,
    ass_company_id: 0,
    ass_branch_id: 0,
    ass_ar_id: 0,
    ass_code: "",
    ass_address: "",
    ass_district_sub: "",
    ass_district: "",
    ass_province: "",
    ass_post_code: "",
    ass_country: "",
    ass_landmarks: "",
    ass_telephone: "",
    ass_map_location_x: "",
    ass_map_location_y: "",
    objAddress: [],
    AddressShow: false,
    AddressAdd: false,
    searchsale: false,
    sale_id: JSON.parse(sessionStorage.userid),
    salecode: "",
    salename: "",
    salemock: "",
    searchsaleobj: [],
    validity: 1,
    is_condition_send: 0,
    my_description: "",
    creator_by: "",
    branch_id: 0,
    docnoid: sessionStorage.iddocno,
    answer_cus: "",
    company_id: sessionStorage.company_id,
    php: "https://" + document.domain,
    ar_bill_address: "",
    ar_telephone: "",
    department: "",
    searchdepart: false,
    objdepart: [],
    project: "",
    idprojectC: "",
    searchproject: false,
    objproject: [],
    Allocate: "",
    searchAllocate_m: false,
    objAllocate: [],
    Allocateid: "",
    searchunitcode_m: false,
    unitcode_obj: [],
    thisunticode: [],
    searchcustcontact: false,
    custcontact: "",
    objCustContact: [],
    stockobj: [],
    namestock: "",
    stockall: [],
    searchwarehousecode_m: false,
    stock_obj: [],
    stock_index: "",
    cart_item_code: [],
    barcode_unitcode: "",
    isLoading: false,
    fullPage: true,
    index: 0,
    contact: "",
    //ข้อมูลลูกค้า
    cus_name: "",
    cus_timetorecive: "",
    cus_regis: "",
    cus_transport: "",
    cus_etc: "",
    //ที่อยู่ลูกค้า
    cus_address: "",
    cus_district: "",
    cus_canton: "",
    cus_province: "",
    cus_post: "",
    cus_tel: "",
    qtkeyword: "",
    collectQT: [],
    permission: JSON.parse(sessionStorage.Datauser).menu,
    pattern_order: ""
  }),

  methods: {
    checkpermission() {
      console.log(1);
      let id = 99;
      if (this.typepage == "IV") {
        id = 8;
      } else if (this.typepage == "SO") {
        id = 1;
      } else if (this.typepage == "DS") {
        id = 4;
      } else if (this.typepage == "QT") {
        id = 0;
      }
      if (!api.checkpermission(1)) {
        console.log(1);
        this.$router.push({ name: "index" });
      }
    },
    removeitemtable(index) {
      console.log(
        JSON.stringify(this.searchProductInObject(this.dproducts, index))
      );
      this.dproducts.splice(
        this.searchProductInObject(this.dproducts, index),
        1
      );
    },
    searchstorecode(val) {
      console.log("*********>" + JSON.stringify(val));
      let payload = {
        item_code: val.item_code
      };
      this.isLoading = true;
      // console.log(payload)
      api.searchunitcode(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.data));
          this.stock_index = val.index;
          console.log(this.stock_index);
          this.stock_obj = result.data[0].stk_location;
          this.searchwarehousecode_m = true;
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูล Unit code ผิดพลาด");
        }
      );
    },
    searchunticode(val) {
      this.barcode_unitcode = val.bar_code;
      let payload = {
        item_code: val.item_code
      };

      this.stock_index = val.index;
      this.isLoading = true;
      // console.log(payload)
      api.searchunitcode(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.data));
          this.unitcode_obj = result.data;
          this.searchunitcode_m = true;
          //  console.log('Dproducts : //'+JSON.stringify(this.dproducts))
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูล Unit code ผิดพลาด");
        }
      );
    },
    checkitem() {
      console.log(this.dproducts);
    },
    selectunitcode_step2(val) {
      console.log(JSON.stringify(val));
      this.searchunitcode_m = false;
      console.log(this.stock_index);
      var index = this.stock_index;
      console.log("index  :  " + index);

      if (this.billtype == 0) {
        //สด
        this.dproducts[index].unit_code = val.unit_code;
        this.dproducts[index].price = val.sale_price_1;
        this.dproducts[index].packing_rate_1 = val.rate_1;
        this.dproducts[index].item_amount =
          this.dproducts[index].price * this.dproducts[index].qty -
          this.dproducts[index].discount_word;
      }
      if (this.billtype == 1) {
        //เชื่อ
        this.dproducts[index].unit_code = val.unit_code;
        this.dproducts[index].price = val.sale_price_2;
        this.dproducts[index].packing_rate_1 = val.rate_1;
        this.dproducts[index].item_amount =
          this.dproducts[index].price * this.dproducts[index].qty -
          this.dproducts[index].discount_word;
      }
    },
    selectwarehousecode(val) {
      console.log(JSON.stringify(val));
      this.searchwarehousecode_m = false;
      this.dproducts[this.stock_index].wh_code = val.wh_code;
      this.dproducts[this.stock_index].shelf_code = val.shelf_code;
      this.dproducts[this.stock_index].stocklimit = val.qty;
    },
    findWithAttr(array, attr, value) {
      for (var i = 0; i < array.length; i += 1) {
        if (array[i][attr] === value) {
          return i;
        }
      }
      return -1;
    },
    searchAllocate() {
      let payload = {
        keyword: this.Allocate
      };
      this.isLoading = true;
      console.log(payload);
      api.searchAllocate(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.data));
          if (result.data.length == 0) {
            alertify.error("ไม่มีการจัดสรร");
            return;
          }
          if (result.data.length == 1) {
            this.Allocateid = result.data[0].id;
            this.Allocate = result.data[0].name;
          } else if (result.data.length > 1) {
            this.searchAllocate_m = true;
            this.objAllocate = result.data;
          }
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลการจัดสรรผิดพลาด");
        }
      );
    },
    selectAllocate_step2(val) {
      this.Allocateid = val.id;
      this.Allocate = val.name;
      this.searchAllocate_m = false;
    },
    searchproject_step2() {
      let payload = {
        keyword: this.project
      };
      console.log(payload);
      this.isLoading = true;
      api.searchproject(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.data));
          // console.log(result.data.length)
          if (result.data.length == 0) {
            alertify.error("ไม่มีโครงการนี้");
            return;
          }
          if (result.data.length == 1) {
            this.project = result.data[0].code + " " + result.data[0].name;
            this.idprojectC = result.data[0].id;
          } else if (result.data.length > 1) {
            this.searchproject = true;
            this.objproject = result.data;
          }
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลโครงการผิดพลาด");
        }
      );
    },
    selectproject_step2(val) {
      // console.log(val)
      this.idprojectC = val.id;
      this.project = val.code + " " + val.name;
      this.searchproject = false;
    },
    searchdepart_step2() {
      let payload = {
        keyword: this.department
      };
      this.isLoading = true;
      console.log(payload);
      api.searchdepartment(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.data));
          // console.log(result.data.length)
          if (result.data.length == 0) {
            alertify.error("ไม่มีแผนกนี้");
            return;
          }
          if (result.data.length == 1) {
            this.iddepartment = result.data[0].id;
            this.department = result.data[0].name;
          } else if (result.data.length > 1) {
            this.searchdepart = true;
            this.objdepart = result.data;
          }
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลแผนกผิดพลาด");
        }
      );
    },
    selectdepart_step2(val) {
      this.iddepartment = val.id;
      this.searchdepart = false;
      this.department = val.name;
    },
    selectJobById() {
      this.job_id = this.job_id;
    },
    selectRefNoById() {
      this.ref_no = this.ref_no;
    },
    isshowdoc_fuc() {
      if (!this.isshowdocument) {
        this.docheight = "72px";
      }
      if (this.isshowdocument) {
        this.docheight = "256px";
      }
    },
    showAddrassFuc() {
      if (!this.showAddrass) {
        this.addressHeight = "72px";
      }
      if (this.showAddrass) {
        this.addressHeight = "256px";
      }
    },
    SearchIdAddress() {
      let payload = {
        id: this.ass_id
      };
      console.log(payload);
      if (payload.id == 0) {
        return;
      }
      this.isLoading = true;
      api.SearchAddressById(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result));
          (this.ass_id = result.data[0].id),
            (this.ass_company_id = result.data[0].company_id),
            (this.ass_branch_id = result.data[0].branch_id),
            (this.ass_ar_id = result.data[0].ar_id),
            (this.ass_code = result.data[0].code),
            (this.ass_address = result.data[0].address),
            (this.ass_district_sub = result.data[0].district_sub),
            (this.ass_district = result.data[0].district),
            (this.ass_province = result.data[0].province),
            (this.ass_post_code = result.data[0].post_code),
            (this.ass_country = result.data[0].country),
            (this.ass_landmarks = result.data[0].landmarks),
            (this.ass_telephone = result.data[0].telephone),
            (this.ass_map_location_x = result.data[0].map_location_x),
            (this.ass_map_location_y = result.data[0].map_location_y);
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลแผนกผิดพลาด");
        }
      );
    },
    SearchKeywordAddress() {
      let payload = {
        keyword: String(this.idcus)
      };
      console.log(payload);
      this.isLoading = true;
      api.SearchAddressByKeyword(
        payload,
        result => {
          console.log(result);
          this.isLoading = false;
          this.objAddress = result.data;
          this.AddressShow = true;
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลแผนกผิดพลาด");
        }
      );
    },
    selectAddress(val) {
      (this.ass_id = val.id),
        (this.ass_company_id = val.company_id),
        (this.ass_branch_id = val.branch_id),
        (this.ass_ar_id = val.ar_id),
        (this.ass_code = val.code),
        (this.ass_address = val.address),
        (this.ass_district_sub = val.district_sub),
        (this.ass_district = val.district),
        (this.ass_province = val.province),
        (this.ass_post_code = val.post_code),
        (this.ass_country = val.country),
        (this.ass_landmarks = val.landmarks),
        (this.ass_telephone = val.telephone),
        (this.ass_map_location_x = val.map_location_x),
        (this.ass_map_location_y = val.map_location_y);
      this.AddressShow = false;
    },
    createAddressSO() {
      let payload = {
        id: 0,
        company_id: this.ass_company_id,
        branch_id: this.ass_branch_id,
        ar_id: this.ass_ar_id,
        code: this.ass_code,
        address: this.ass_address,
        district_sub: this.ass_district_sub,
        district: this.ass_district,
        province: this.ass_province,
        post_code: this.ass_post_code,
        country: this.ass_country,
        landmarks: this.ass_landmarks,
        telephone: this.ass_telephone,
        map_location_x: this.ass_map_location_y,
        map_location_y: this.ass_map_location_y
      };
      console.log(payload);
      this.isLoading = true;
      api.createAddress(
        payload,
        result => {
          console.log(result);
          this.isLoading = false;
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลแผนกผิดพลาด");
        }
      );
    },
    calexpire_Date() {
      var date1 = new Date(this.expiredate_cal);
      this.expiredate_cal =
        date1.getMonth() +
        1 +
        "/" +
        date1.getDate() +
        "/" +
        date1.getFullYear();
      var date2 = new Date();
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      this.expire_date = diffDays;
    },
    calexpiredate() {
      console.log(this.expire_date);
      let date = new Date();
      date.setDate(date.getDate() + parseInt(this.expire_date));
      this.expiredate_cal =
        date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
    },
    calDueDate_date() {
      var date1 = new Date(this.DueDate_date);
      this.DueDate_date =
        date1.getMonth() +
        1 +
        "/" +
        date1.getDate() +
        "/" +
        date1.getFullYear();
      // alert(this.DueDate_date)
      var date2 = new Date();
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      this.Deliver_date = diffDays;
    },
    calDeliverdate() {
      let date = new Date();
      date.setDate(date.getDate() + parseInt(this.Deliver_date));
      this.DueDate_date =
        date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
      console.log(this.DueDate_date);
    },
    calbathordiscount() {
      // alert('dsad')
      console.log(this.percal);
      this.percal = !this.percal;
    },
    // newUser() {
    //   this.$refs.addproduct.$el.focus()
    // },
    searchOnTable() {
      this.searched = searchByName(this.dproducts, this.search);
      console.log(this.searched);
    },
    tests() {
      Swal.fire({
        type: "error",
        title: "Maintenance",
        text: "The Process Of Maintenance "
      });
      // alert(JSON.stringify(this.objuser.menu[0].is_create));
      // console.log(JSON.stringify(this.objuser.menu))
    },
    setDone(id, index) {
      if (id == "first") {
        console.log(
          "tag",
          this.tablecode,
          this.billtype,
          this.taxtype,
          this.idcus,
          this.dproducts.length
        );
        if (
          this.tablecode == null ||
          this.billtype == null ||
          this.taxtype == null ||
          this.idcus == null ||
          this.dproducts.length <= 0
        ) {
          alert("กรุณากรอกข้อมูลให้ครบ");
          return;
        }
      }
      if (id == "third") {
        this.$router.push("/solist");
        return;
      }
      //
      this[id] = true;
      this.secondStepError = null;
      //

      //บันทึก

      if (index) {
        this.active = index;
      }
      document.getElementsByClassName("md-content")[0].scrollTop = 0;
    },
    saveDocument() {
      this.isLoading = true;
      let doc_type;
      let tax_type;
      let percent;
      let discount_amount;

      if (this.tablecode == "SO") {
        doc_type = 0;
      } else if (this.tablecode == "RO") {
        doc_type = 1;
      }

      var str = this.salecode;
      var res = str.split("/");
      // console.log(res)
      // var sale_code = res[0]
      // var sale_name = res[1]

      if (this.percal) {
        percent = "%";
        discount_amount =
          this.totalprice -
          (this.totalprice - (this.totalprice * this.caldiscount) / 100);
        //   alert('dsa')
      } else if (!this.percal) {
        percent = "";
        discount_amount = this.caldiscount;
      }
      // console.log(this.datenow_datepicker)
      // console.log(this.docnoid)
      this.showdocno();
      Swal.fire({
        title: "คุณต้องการบันทึกใบสั่งขายนี้?",
        text: "ยืนยันการทำใบสั่งขาย!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "ตกลง",
        cancelButtonText: "ยกเลิก"
      }).then(result => {
        if (result.value) {
          for(let i=0;this.dproducts.length>i;i++){
            this.dproducts[i].line_number=i+1;
          }
          let payload = {
            id: parseInt(this.docnoid), // 0 แก้ไข,update ตามไอดี
            branch_id: this.branch_id,
            doc_no: this.docno,
            //norecord
            ar_bill_address: this.ar_bill_address,
            ar_telephone: this.ar_telephone,
            datenow_datepicker: this.datenow_datepicker,
            dif_fee: this.dif_fee,
            //norecord

            doc_type,
            ar_id: this.idcus,
            ar_code: this.searchcus,
            ar_name: this.detailcus,
            sale_id: this.sale_id,
            sale_code: this.salecode,
            sale_name: this.salename,
            bill_type: parseInt(this.billtype),
            tax_type: parseInt(this.taxtype),
            tax_rate: 7,
            depart_code: "",
            ref_no: this.ref_no,
            job_id: this.job_id,
            delivery_id: parseInt(this.ass_id),
            is_confirm: parseInt(this.isConfirm),
            bill_status: 0,
            credit_day: this.bill_credit,
            due_date: this.convermonth_y_m_d(this.DueDate_cal),
            validity: parseInt(this.validity),
            expire_credit: parseInt(this.expire_date),
            expire_date: this.convermonth_y_m_d(this.expiredate_cal),
            delivery_day: parseInt(this.Deliver_date),
            delivery_date: this.convermonth_y_m_d(this.DueDate_date),
            is_condition_send: parseInt(this.is_condition_send),
            my_description: this.infoNotice,
            sum_of_item_amount: this.totalprice,
            discount_word: this.caldiscount + percent,
            discount_amount: parseInt(discount_amount),
            after_discount_amount: this.totalprice - this.caldiscount,
            company_id: parseInt(this.company_id),
            before_tax_amount: this.totalprice,
            assert_status: parseInt(this.answer_cus),
            depart_id: parseInt(this.iddepartment),
            project_id: parseInt(this.idprojectC),
            allocate_id: parseInt(this.Allocateid),
            is_cancel: parseInt(this.isCancel),
            creator_by: this.creator_by,
            contact_id: this.custcontactid,
            subs: this.dproducts,
            discount_select: this.discountSelect
          };
          //print zone
          document.getElementsByName("datasale")[0].value = JSON.stringify(
            payload
          );

          document.getElementsByName("datasale")[1].value = JSON.stringify(
            payload
          )
          console.log(JSON.stringify(payload))
          // console.log(JSON.stringify(payload))
          api.createsale(
            payload,
            result => {
              console.log(result);
              if ((result.response = "success")) {
                this.docnoid = result.data.id;
                setTimeout(() => {
                  this.setDone("second", "third");
                  console.log(result);
                  alertify.success("บันทึกสำเร็จ " + this.docno);
                  this.isLoading = false;
                }, 500);
              }
              if (result.error) {
                // alertify.error('เกิดข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้');
                Swal.fire({
                  type: "error",
                  title: result.error
                });
                this.isLoading = false;
                return;
              }
            },
            error => {
              console.log(JSON.stringify(error));
              //Customerall
              alertify.error("เกิดข้อผิดพลาด");
              this.isLoading = false;
            }
          );
        }
        this.isLoading = false;
      });
    },
    convermonth_y_m_d(val) {
      if (val == "") {
        return "";
      }
      var date = val;
      var cut = date.split("/");
      var result = cut[2] + "-" + cut[0] + "-" + cut[1];
      return result;
    },
    convertmonth_d_m_y(val) {
      var date = val.substring(0, 10);
      var cut = date.split("-");
      var result = cut[1] + "/" + cut[2] + "/" + cut[0];
      return result;
    },
    fsearchcus() {
      var payload = {
        keyword: this.searchcus
      };
      this.isLoading = true;
      api.Customerall(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.member));
          if (result.member.length == 0) {
            alertify.error("ไม่มีข้อมูลลูกค้านี้");
            return;
          }
          if (result.member.length == 1) {
            this.idcus = result.member[0].id;
            this.detailcus = result.member[0].name;
            this.searchcus = result.member[0].code;
            this.creditday;
            this.changePriceType();
            this.selectCustomer = true;
          } else if (result.member.length > 1) {
            this.detailcusall = result.member;
            this.showDialogcus = true;
          }
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("member ข้อมูลค้นหาลูกค้าผิดพลาด");
        }
      );
    },
    searchCustomerRT() {
      var payload = {
        keyword: this.searchcus
      };

      api.Customerall(
        payload,
        result => {
          console.log(JSON.stringify(result.member));
          if (result.member.length == 0) {
            alertify.error("ไม่มีข้อมูลลูกค้านี้");
            return;
          }
          if (result.member.length == 1) {
            this.idcus = result.member[0].id;
            this.detailcus = result.member[0].name;
            this.searchcus = result.member[0].code;
            this.customerLevel = result.member[0].price_level;
            this.bill_credit = result.member[0].bill_credit;
            var date = new Date();
            date.setDate(date.getDate() + this.bill_credit);
            this.DueDate_cal =
              date.getMonth() +
              1 +
              "/" +
              date.getDate() +
              "/" +
              date.getFullYear();
          } else if (result.member.length > 1) {
            this.detailcusall = result.member;
            this.showDialogcus = true;
          }
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลค้นหาลูกค้าผิดพลาด");
        }
      );
    },
    C_customer(val) {
      console.log(JSON.stringify(val));
      this.idcus = val.id;
      this.searchcus = val.code;
      this.detailcus = val.name;

      this.showDialogcus = false;
      //bill_credit
      this.customerLevel = val.price_level;
      this.bill_credit = val.bill_credit;
      //
      var date = new Date();
      console.log(date);
      date.setDate(date.getDate() + this.bill_credit);
      this.DueDate_cal =
        date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
      console.log(this.DueDate_cal);
      this.changePriceType();
      this.selectCustomer = true;
    },
    addproduct() {
      this.isLoading = true;
      console.log(this.keywordproduct);
      if (this.billtype === "" && this.billtype !== 0 && this.billtype !== 1) {
        if (this.attention == "wobble-hor-bottom") {
          this.attention = "wobble-hor-bottom2";
        } else {
          this.attention = "wobble-hor-bottom";
        }
        return;
      }

      if (!this.keywordproduct) {
        return;
      }

      let payload = {
        keyword: this.keywordproduct
      };
      this.isLoading = true;
      console.log(payload);
      api.searchbykeyword(
        payload,
        result => {
          this.isLoading = false;
          console.log(result.data);
          console.log(result.data.length);
          this.showDialogproduct = true;

          this.dataproductDialog = result.data;
          console.log(this.dataproductDialog);
          this.stockall = result.data[0].stk_location;
          console.log(this.stockall);
          console.log("billtype : " + this.billtype);
          this.$refs.addproduct.$el.focus();
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("ข้อมูล สินค้าเกิดข้อผิดพลาด");
        }
      );
    },
    addproductrt() {
      console.log(this.keywordproduct);
      if (this.billtype === "" && this.billtype !== 0 && this.billtype !== 1) {
        if (this.attention == "wobble-hor-bottom") {
          this.attention = "wobble-hor-bottom2";
        } else {
          this.attention = "wobble-hor-bottom";
        }
        return;
      }

      if (!this.keywordproduct) {
        return;
      }

      let payload = {
        keyword: this.keywordproduct
      };
      console.log(payload);
      api.searchbykeyword(
        payload,
        result => {
          console.log(result.data);
          console.log(result.data.length);
          this.showDialogproduct = true;

          this.dataproductDialog = result.data;
          console.log(this.dataproductDialog);
          this.stockall = result.data[0].stk_location;
          console.log(this.stockall);
          console.log("billtype : " + this.billtype);
          this.$refs.addproduct.$el.focus();
        },
        error => {
          console.log(JSON.stringify(error));
          alertify.error("ข้อมูล สินค้าเกิดข้อผิดพลาด");
        }
      );
    },
    selectedProduct(index) {
      var product = {
        id: this.dataproductDialog[index].id,
        item_code: this.dataproductDialog[index].item_code,
        item_name: this.dataproductDialog[index].item_name,
        bar_code: this.dataproductDialog[index].bar_code,
        unit_code: this.dataproductDialog[index].unit_code,
        sale_price_1: this.dataproductDialog[index].sale_price_1,
        sale_price_2: this.dataproductDialog[index].sale_price_2,
        rate_1: this.dataproductDialog[index].rate_1,
        pic_path_1: this.dataproductDialog[index].pic_path_1
      };
      this.dproducts.push(product);
      console.log(this.dproducts);
      this.showDialogproduct = false;
    },
    removeProduct(index) {
      // console.log(JSON.stringify(this.dproducts.length))
      // this.searchProductInObject(this.dproducts,index)
      console.log(
        JSON.stringify(this.searchProductInObject(this.dproducts, index))
      );
      this.dproducts.splice(
        this.searchProductInObject(this.dproducts, index),
        1
      );
    },
    searchProductInObject(arraytosearch, valuetosearch) {
      for (var i = 0; i < arraytosearch.length; i++) {
        if (arraytosearch[i].index == valuetosearch) {
          return i;
        }
      }
      return null;
    },
    showdocno() {
      if (this.docnoid != 0) {
        return;
      }
      if (!this.tablecode || !this.billtype) {
        return;
      }

      this.disablebilltype = true;
      let payload = {
        branch_id: this.objuser.branch_id,
        table_code: this.tablecode,
        bill_type: parseInt(this.billtype)
      };
      console.log(JSON.stringify(payload));
      this.isLoading = true;
      api.showdocno(
        payload,
        result => {
          this.isLoading = false;
          if (result.error) {
            this.docno = "ไม่มีข้อมูล";
            return;
          }
          this.docno = result;
          console.log("docno", this.docno);
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          //Customerall
          alertify.error("ข้อมูล ประเภทเสนอราคาเกิดข้อผิดพลาด");
          //  alertify.success('Error login');
          // this.cload()
        }
      );
    },
    mockDocNo() {
      if (!this.tablecode || !this.billtype) {
        return;
      }

      if (this.dproducts.length > 0) {
      }

      this.disablebilltype = true;
      let payload = {
        branch_id: this.objuser.branch_id,
        table_code: this.tablecode,
        bill_type: parseInt(this.billtype)
      };
      this.isLoading = true;
      console.log(payload);
      api.showdocno(
        payload,
        result => {
          this.isLoading = false;
          if (result.error) {
            this.mockdocno = "ไม่มีข้อมูล";
            return;
          }
          this.mockdocno = "";
          for (var i = 0; i < result.length - 4; i++) {
            this.mockdocno += result.charAt(i);
          }
          this.mockdocno += "XXXX";
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("ข้อมูล ประเภทเสนอราคาเกิดข้อผิดพลาด");
        }
      );
    },
    // showdetail(val, stock, indexstock) {
    //   console.log("----->" + JSON.stringify(val))
    //   console.log(stock)
    //   if (this.billtype == 0) {
    //     var datashow = {
    //       index: this.index++,
    //       item_id: val.id,
    //       item_code: val.item_code,
    //       bar_code: val.bar_code,
    //       item_name: val.item_name,
    //       unit_code: val.unit_code,
    //       qty: 1,
    //       price: val.sale_price_1,
    //       sale_price_1: val.sale_price_1,
    //       sale_price_2: val.sale_price_2,
    //       discount_word: '0',
    //       discount_amount: 0,
    //       item_amount: val.sale_price_1 * 1,
    //       item_description: "",
    //       packing_rate_1: parseInt(val.rate_1),
    //       is_cancel: 0,
    //       wh_code: val.wh_code,
    //       stock_type: val.stock_type,//แก้
    //       // wh_code: val.stk_location[indexstock].wh_code, //แก้
    //       shelf_code: val.stk_location[indexstock].shelf_code, //แก้
    //       stocklimit: val.stk_location[indexstock].qty,  //แก้
    //     }
    //     this.dproducts.push(datashow)
    //     console.log(JSON.stringify(datashow))
    //     //close modal
    //     this.showDialogproduct = false
    //     alertify.success('เพิ่มข้อมูลสินค้า ' + val.item_name);
    //   }
    //   else if (this.billtype == 1) {
    //     var datashow = {
    //       index: this.index++,
    //       item_id: val.id,
    //       item_code: val.item_code,
    //       bar_code: val.bar_code,
    //       item_name: val.item_name,
    //       unit_code: val.unit_code,
    //       qty: 1,
    //       price: val.sale_price_1,
    //       sale_price_1: val.sale_price_1,
    //       sale_price_2: val.sale_price_2,
    //       discount_word: '0',
    //       discount_amount: 0,
    //       item_amount: val.sale_price_1 * 1,
    //       item_description: "",
    //       packing_rate_1: parseInt(val.rate_1),
    //       is_cancel: 0,
    //       wh_code: val.wh_code,
    //       stock_type: val.stock_type, //แก้
    //       // wh_code: val.stk_location[indexstock].wh_code, //แก้
    //       shelf_code: val.stk_location[indexstock].shelf_code, //แก้
    //       stocklimit: val.stk_location[indexstock].qty,  //แก้
    //     }
    //     this.dproducts.push(datashow)
    //     //close modal
    //     this.showDialogproduct = false
    //     alertify.success('เพิ่มข้อมูลสินค้า ' + val.item_name);
    //   }
    //   this.keywordproduct = ''
    //   console.log(JSON.stringify(this.dproducts))
    //   //console.log(datashow)
    // },
    calculatedata(val) {
      let qty_total = parseInt(val.qty) * parseInt(val.packing_rate_1);
      // console.log(JSON.stringify(val))
      let index = val.index;
      // console.log(JSON.stringify(this.dproducts))

      if (this.dproducts[index].stock_type == 0) {
        // เช็คว่าเป็นสินค้า ,0 เป็นสินค้า ,1 เป็นบริการ
        if (qty_total > this.dproducts[index].stocklimit) {
          alert("คุณระบุจำนวนสิ้นค้าเกินกว่าที่คลังมี");
          this.dproducts[index].qty = 0;
          return;
        }
      }
      val.discount_word = val.discount_word.toString();
      // console.log(val.discount_word)

      if (val.discount_word.search(",") < 0) {
        if (val.discount_word.slice(-1) == "%") {
          var cutper = parseInt(val.discount_word.slice(0, -1));
          val.item_amount = val.qty * (val.price - (val.price * cutper) / 100);
          val.discount_amount =
            val.price * val.qty -
            (val.price - (val.price * cutper) / 100) * val.qty;
          console.log(val.discount_word); // ตัวอักษร
          console.log(val.discount_amount); // ส่วนต่าง
          return;
        } else {
          val.discount_amount =
            val.price * val.qty - (val.price - val.discount_word) * val.qty;
        }
        console.log(JSON.stringify(val));
        if (this.billtype == 0) {
          //เงินสด
          val.item_amount = val.qty * (val.price - val.discount_word);
        } else if (this.billtype == 1) {
          //เงินเชื่อ
          val.item_amount = val.qty * (val.price - val.discount_word);
        }
        console.log(val.discount_word); // ตัวอักษร
        console.log(val.discount_amount); // ส่วนต่าง
      } else if (val.discount_word.search(",") >= 0) {
        var res = val.discount_word.split(",");
        if (res[0].slice(-1) == "%") {
          var cutper = parseInt(res[0].slice(0, -1));
          val.item_amount = val.price - (val.price * cutper) / 100;
          var diff1 = (val.price * cutper) / 100;
          console.log("diff1 : " + diff1);
        } else {
          var diff1 = val.price - (val.price - res[0]);
          console.log(diff1);
          val.item_amount = val.price - res[0];
        }
        if (res[1].slice(-1) == "%") {
          let cutper1 = parseInt(res[1].slice(0, -1));
          val.item_amount =
            val.qty * (val.item_amount - (val.item_amount * cutper1) / 100);
          var diff2 = ((val.price - diff1) * cutper1) / 100;
          console.log("diff2 : " + diff2);
          val.discount_amount = (diff1 + diff2) * val.qty;
          console.log(val.discount_amount);
        } else {
          var diff2 = val.price - (val.price - res[1]);
          val.discount_amount = (diff1 + diff2) * val.qty;
          val.item_amount = val.price * val.qty - val.discount_amount;
          console.log(val.discount_amount);
        }
        return;
      }

      console.log("**** qty *****" + JSON.stringify(val.qty));
    },
    getFocus(id) {
      document.getElementById(id).focus();
    },
    convertmoney(val) {
      // console.log(val)
      var number = numeral(val).format("0,0.00");
      return number;
    },
    convertshowdoc(val) {
      if (val == false) {
        var comment = "ปิด";
        return comment;
      } else if (val == true) {
        var comment = "เปิด";
        return comment;
      }
    },
    showcontent_step2() {
      if (this.docnoid == 0) {
        this.salecode = this.objuser.sale_code;
        this.salename = this.objuser.username;
        this.salemock = this.objuser.sale_code + "/" + this.objuser.username;
        this.selectSale = true;
      }
    },
    searchsale_step2() {
      let payload = {
        keyword: this.salecode
      };
      this.isLoading = true;
      console.log(payload);
      api.searchcus(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.data));
          // console.log(result.data.length)
          if (result.data.length == 0) {
            alertify.error("ไม่มีพนักงานคนนี้");
            return;
          }
          if (result.data.length == 1) {
            this.salecode = result.data[0].sale_code;
            this.salename = result.data[0].sale_name;
            this.selectSale = true;
            this.salemock = this.salecode + "/" + this.salename;
          } else if (result.data.length > 1) {
            this.searchsale = true;
            this.searchsaleobj = result.data;
          }
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลค้นหาลูกค้าผิดพลาด");
        }
      );
    },
    getItems(keyword, s_type, i) {
      // var price_type;
      api.findproductByKeyword(
        { keyword: keyword, sale_type: s_type },
        result => {
          if (this.customerLevel == 1 || this.customerLevel == "") {
            this.dproducts[i].price = result.data[0].sale_price_1;
            this.dproducts[i].item_amount =
              (result.data[0].sale_price_1 - this.dproducts[i].discount_word) *
              this.dproducts[i].qty;
          } else if (this.customerLevel == 2) {
            this.dproducts[i].price = result.data[0].sale_price_2;
            this.dproducts[i].item_amount =
              (result.data[0].sale_price_2 - this.dproducts[i].discount_word) *
              this.dproducts[i].qty;
          }
        },
        error => {
          // alertify.error(
          //     "ของในคลังหมด"
          //   );
        }
      );
    },
    // เลือกประเภทใบสั่งขาย
    changePriceType() {
      if (this.docnoid == 0) {
        for (var i = 0; i < this.dproducts.length; i++) {
          for (var i = 0; i < this.dproducts.length; i++) {
            console.log(JSON.stringify(this.dproducts[i]));
            if (this.billtype == 0) {
              this.getItems(this.dproducts[i].item_code, 0, i);
            }
            if (this.billtype == 1) {
              this.getItems(this.dproducts[i].item_code, 1, i);
            }
            console.log(JSON.stringify("sdfsdfsdf " + this.dproducts[i].price));
            console.log(JSON.stringify(this.dproducts[i]));
          }
        }
      }
    },
    selectcus_step2(val) {
      console.log(JSON.stringify(val));
      this.sale_id = val.employee_id;
      this.salecode = val.sale_code;
      this.salename = val.sale_name;
      this.salemock = this.salecode + "/" + this.salename;
      this.searchsale = false;
      this.selectSale = true;
    },
    searchCustContact() {
      let payload = {
        id: this.idcus,
        keyword: this.custcontact
      };
      console.log(payload);
      this.isLoading = true;
      api.searchCustContactByKeyword(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.data));
          if (result.data.length == 0) {
            alertify.error("ไม่พบผู้ติดต่อ");
            return;
          }
          // if (result.data.length == 1) {
          //   this.custcontactid = result.data[0].id
          //   this.custcontact = result.data[0].contact_code + ' ' + result.data[0].contact_name

          // } else if (result.data.length > 1) {
          if (result.data.length > 0) {
            this.searchcustcontact = true;
            this.objCustContact = result.data;
          }
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลผู้ติดต่อผิดพลาด");
        }
      );
    },
    selectCustContact_step2(val) {
      this.custcontactid = val.id;
      this.searchcustcontact = false;
      this.custcontact = val.contact_code + " " + val.contact_name;
    },
    showCustContactById() {
      let payload = {
        id: this.custcontactid
      };
      console.log("payload showCustContactById ", payload);
      if (payload.id == 0) {
        return;
      }
      api.searchCustContactById(
        payload,
        result => {
          this.custcontactid = result.data[0].id;
          this.custcontact =
            result.data[0].contact_code + " " + result.data[0].contact_name;
          console.log(JSON.stringify(result));
        },
        error => {
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลผิดพลาด");
        }
      );
    },
    focussearchcus() {
      this.salecode = "";
      this.$refs.codesale.$el.focus();
    },
    showedit() {
      if (this.docnoid == 0) {
        // alert('หนักหลัก')
        return;
      }
      // alert('แก้ไข')
      // แก้ไข
      let payload = {
        id: parseInt(this.docnoid)
      };
      this.isLoading = true;
      console.log(payload);
      api.detailsaleall(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.data));
          console.log(result.data.bill_type);
          let doc_type;
          let tax_type;
          // let percent
          // let discount_amount

          if (result.data.doc_type == 0) {
            doc_type = "SO";
          } else if (result.data.doc_type == 1) {
            doc_type = "RO";
          }
          // this.dproducts = []
          this.customerLevel = result.data.price_level;
          this.infoNotice = result.data.my_description;
          this.disablebilltype = true;
          this.tablecode = doc_type;
          this.billtype = result.data.bill_type;
          this.ar_bill_address = result.data.ar_bill_address;
          this.ar_telephone = result.data.ar_telephone;
          this.docno = result.data.doc_no;
          this.taxtype = result.data.tax_type;
          this.datenow_datepicker = result.data.doc_date;
          this.idcus = result.data.ar_id;
          this.searchcus = result.data.ar_code;
          this.detailcus = result.data.ar_name;
          this.depart_id = result.data.depart_id;
          this.Allocateid = result.data.allocate_id;
          this.idprojectC = result.data.project_id;
          this.custcontactid = result.data.contact_id;
          this.ref_no = result.data.ref_no;
          this.job_id = result.data.job_id;
          this.ass_id = result.data.delivery_id;
          var datasubs = result.data.subs;
          let data;
          console.log(datasubs);
          for (let x = 0; x < datasubs.length; x++) {
            data = {
              item_id: datasubs[x].id,
              item_code: datasubs[x].item_code,
              bar_code: datasubs[x].bar_code,
              item_name: datasubs[x].item_name,
              unit_code: datasubs[x].unit_code,
              qty: datasubs[x].qty,
              price: datasubs[x].price,
              discount_word: datasubs[x].discount_word,
              discount_amount: datasubs[x].discount_amount,
              item_amount: datasubs[x].item_amount,
              item_description: datasubs[x].item_description,
              packing_rate_1: datasubs[x].packing_rate_1,
              wh_code: datasubs[x].wh_code,
              quo_id: datasubs[x].quo_id,
              is_cancel: datasubs[x].is_cancel,
              line_number: datasubs[x].line_number,
              ref_line_number: datasubs[x].ref_line_number
            };
            this.dproducts.push(data);
          }
          console.log(JSON.stringify(this.dproducts));
          this.salecode = result.data.sale_code;
          this.salename = result.data.sale_name;
          this.salemock = this.salecode + "/" + this.salename;
          this.selectSale = true;
          this.validity = result.data.validity;
          this.showDepartment();
          this.showprojectById();
          this.showAllocateById();
          this.showCustContactById();
          this.SearchIdAddress();
          this.expire_date = result.data.expire_credit;
          this.caldiscount = result.data.discount_amount;
          // console.log(this.expire_date)
          this.answer_cus = result.data.assert_status;
          this.Deliver_date = result.data.delivery_day;
          this.bill_credit = result.data.credit_day;
          this.is_condition_send = result.data.is_condition_send;
          this.my_description = result.data.my_description;
          // this.expiredate_cal = this.convertmonth_d_m_y(result.data.expire_date)
          // console.log(this.expiredate_cal)
          this.DueDate_date = this.convertmonth_d_m_y(
            result.data.delivery_date
          );
          this.DueDate_cal = this.convertmonth_d_m_y(result.data.due_date);
          console.log(JSON.stringify(this.DueDate_cal));
          //  console.log(this.dproducts)

          console.log(JSON.stringify(result.data.subs));
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("ข้อมูลผิดพลาด detailquoall");
        }
      );
    },
    showDepartment() {
      let payload = {
        id: this.depart_id
      };
      console.log("payload showDepartment", payload);
      if (payload.id == 0) {
        return;
      }
      api.searchdepartmentById(
        payload,
        result => {
          this.iddepartment = result.data.id;
          this.departmentCode = result.data.code;
          this.department = result.data.name;
          console.log(JSON.stringify(result));
        },
        error => {
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลผิดพลาด");
        }
      );
    },
    showprojectById() {
      let payload = {
        id: this.idprojectC
      };
      console.log("payload showprojectById", payload);
      if (payload.id == 0) {
        return;
      }
      api.searchprojectById(
        payload,
        result => {
          this.idprojectC = result.data.id;
          this.project = result.data.code + " " + result.data.name;
          console.log(JSON.stringify(result));
        },
        error => {
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลผิดพลาด");
        }
      );
    },
    showAllocateById() {
      let payload = {
        id: this.Allocateid
      };
      console.log("payload showAllocateById ", payload);
      if (payload.id == 0) {
        return "";
      }
      api.searchAllocateById(
        payload,
        result => {
          this.Allocateid = result.data.id;
          this.Allocate = result.data.code + " " + result.data.name;
          console.log(JSON.stringify(result));
        },
        error => {
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลผิดพลาด");
        }
      );
    },
    stepOne: function(event) {
      this.setDone("first", "second");
    },
    convertmonth_preview(val) {
      // console.log(val)
      // console.log(val.length)
      if (val.length === undefined) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
          dd = "0" + dd;
        }
        if (mm < 10) {
          mm = "0" + mm;
        }
        today = yyyy + "-" + mm + "-" + dd;
        return today;
      } else if (val.length != undefined) {
        return val.substring(0, 10);
      }
    },
    changevaluetest() {
      this.tablecode = "QT";
    },
    changevaluetest2() {
      this.tablecode = "BO";
    },
    changevaluetest3() {
      this.tablecode = "SO";
    },
    findstock(val, index) {
      console.log(index);

      for (let i = 0; i < this.stockall.length; i++) {
        document.getElementsByClassName("hover" + index)[i].style.visibility =
          "visible";
        document.getElementsByClassName("hover" + index)[i].style.height =
          "35px";
      }
      for (let i = 0; i < this.stockall.length; i++) {
        document.getElementsByClassName("trshow" + index)[i].style.display =
          "table-cell";
      }
      return;
    },
    searchConfirmedQT() {
      let word = {
        keyword: this.searchcus
      };
      this.collectQT = [];
      api.searchQuotationByKeyword(
        word,
        result => {
          console.log(JSON.stringify(result));
          for (var i = 0; i < result.data.length; i++) {
            if (
              result.data[i].is_confirm == 1 &&
              result.data[i].is_cancel == 0
            ) {
              this.collectQT.push(result.data[i]);
            }
          }
          console.log(JSON.stringify(this.collectQT));
          this.selectQTdialog = true;
        },
        error => {
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลผิดพลาด");
          //  alertify.success('Error login');
          // this.cload()
        }
      );
    },
    selectstock(val) {
      alert(JSON.stringify(val));
    },
    checkval() {
      this.datenow_datepicker = "";
    },
    callQTtoSO(i) {
      let payload = {
        id: parseInt(i)
      };
      let sodocno;
      let soid;
      api.transferQTtoSO(
        payload,
        result => {
          console.log(JSON.stringify(result));
          sodocno = { id: result.data.id };
          console.log(JSON.stringify(sodocno));
          this.$router.push({ name: "saleorder2", params: { id: sodocno.id } });
          // location.reload();
        },
        error => {
          console.log(JSON.stringify(error));
          alertify.error("เกิดข้อผิดพลาด ไม่สามารถโอนใบเสนอราคาได้");
        }
      );
      return;
    }
  },
  created() {
    this.searched = this.dproducts;
  },
  computed: {
    keymap() {
      return {
        "ctrl+shift+1": this.changevaluetest,
        "ctrl+shift+2": this.changevaluetest2,
        "ctrl+shift+3": this.changevaluetest3
      };
    },
    totalprice() {
      return this.dproducts.reduce(function(sum, item) {
        return sum + item.item_amount;
      }, 0);
    },
    dif_fee() {
      if (this.taxtype == 0 || this.taxtype == 1) {
        if (!this.percal) {
          return (
            this.totalprice -
            this.caldiscount -
            ((this.totalprice - this.caldiscount) * 100) / 107
          );
        } else if (this.percal) {
          let percent =
            this.totalprice - (this.totalprice * this.caldiscount) / 100;
          console.log(percent);
          return percent - (percent * 100) / 107;
        }
      }
      if (this.taxtype == 2) {
        return 0;
      }
    },
    cal_totalprice() {
      if (this.taxtype == 1) {
        if (!this.percal) {
          return this.totalprice - this.caldiscount;
        }
        if (this.percal) {
          return this.totalprice - (this.totalprice * this.caldiscount) / 100;
        }
      }
      if (this.taxtype == 0) {
        if (!this.percal) {
          return this.totalprice + this.dif_fee;
        }
        if (this.percal) {
          return this.totalprice + this.dif_fee;
        }
      }
      if (this.taxtype == 2) {
        if (!this.percal) {
          return this.totalprice;
        }
        if (this.percal) {
          return this.totalprice;
        }
      }
    },
    firstDayOfAWeek: {
      get() {
        return this.$material.locale.firstDayOfAWeek;
      },
      set(val) {
        this.$material.locale.firstDayOfAWeek = val;
      }
    }
  },
  mounted() {
    this.checkpermission();
    this.docnoid = this.$route.params.id;
    this.showedit();
    this.creator_by = this.objuser.usercode;
    this.branch_id = this.objuser.branch_id;
    this.showcontent_step2();
    // console.log(this.objuser)
    this.calexpiredate();
    this.calDeliverdate();
    // this.changePriceType()
    this.mockDocNo();
  }
};
