const toLower = text => {
  return text.toString().toLowerCase();
};

const searchByName = (items, term) => {
  if (term) {
    return items.filter(
      item =>
        toLower(item.item_name).includes(toLower(term)) ||
        toLower(item.unit_code).includes(toLower(term))
    );
  }
  return items;
};
import Vue from "vue";
import Loading from "vue-loading-overlay";
import "vue-loading-overlay/dist/vue-loading.css";
import Swal from "sweetalert2";
import responsive from "vue-responsive";
import Datepicker from "vuejs-datepicker";
import * as lang from "vuejs-datepicker/src/locale";
import api from "../service/service.js";
import itemtable from "@/components/ui/tableitem";
import searchhiscustomer from "@/components/ui/searchhiscustomer";
import printform from "@/components/ui/docTransferToPrint";
import componentnote from "@/components/ui/componentnote";
import commponenDalivery from "@/components/ui/commponenDalivery";
// import * as jsPDF from 'jspdf'
import searchItem from "@/components/ui/searchItem";
import JQuery from "jquery";
import { setTimeout } from "timers";
let $ = JQuery;
Vue.use(responsive);
export default {
  name: "quotation",
  components: {
    searchhiscustomer,
    Datepicker,
    Loading,
    itemtable,
    searchItem,
    componentnote,
    printform,
    commponenDalivery
  },
  props: {
    ass_id: this.ass_id
  },
  data: () => ({
    msg: "",
    typepage: "quotation",
    selectedDate: null,
    searchhiscustomer: [],
    infoNotice: "",
    date: "",
    search: [],
    searched: "",
    search: "",
    objuser: JSON.parse(sessionStorage.Datauser),
    dproducts: [],
    active: "first",
    first: false,
    second: false,
    third: false,
    secondStepError: null,
    language: "th",
    languages: lang,
    ass_id: 0,
    idcus: "",
    searchcus: "",
    detailcus: "",
    showDialogcus: false,
    showNotice: false,
    textAreaNotice: "",
    noteArr: [],
    detailcusall: [],
    tablecode: "QT",
    billtype: "0",
    taxtype: 1,
    mockdocno: "",
    docno: "ไม่มีข้อมูล",
    keywordproduct: "",
    showDialogproduct: false,
    confirmDialog: false,
    confirmCancel: false,
    confirmConfirm: false,
    confirmTranfer: false,
    isConfirm: 0,
    isCancel: 0,
    selectCustomer: false,
    selectSale: false,
    dataproductDialog: [],
    disablebilltype: false,
    datenow_datepicker: Date.now(),
    attention: "",
    percal: false, //true == % , false == บาท
    caldiscount: 0,
    keywordemp: "",
    discountSelect: true,
    customerLevel: "",
    // page 2
    bill_credit: "",
    DueDate_cal: "",
    Deliver_date: 1,
    DueDate_date: "",
    expire_date: 1,
    expiredate_cal: "",
    isshowdocument: false,
    docheight: "72px",
    searchsale: false,
    sale_id: JSON.parse(sessionStorage.userid),
    salecode: "",
    salename: "",
    salemock: "",
    searchsaleobj: [],
    validity: 1,
    is_condition_send: 0,
    my_description: "",
    creator_by: "",
    branch_id: 0,
    docnoid: sessionStorage.iddocno,
    answer_cus: 0,
    company_id: sessionStorage.company_id,
    php: "https://" + document.domain,
    ar_bill_address: "",
    ar_telephone: "",
    department: "",
    searchdepart: false,
    objdepart: [],
    project: "",
    idprojectC: "",
    searchproject: false,
    objproject: [],
    Allocate: "",
    searchAllocate_m: false,
    objAllocate: [],
    Allocateid: "",
    searchunitcode_m: false,
    searchcustcontact: false,
    custcontact: "",
    objCustContact: [],
    unitcode_obj: [],
    thisunticode: [],
    stockobj: [],
    namestock: "",
    stockall: [],
    isLoading: false,
    fullPage: true,
    permission: JSON.parse(sessionStorage.Datauser).menu[0],
    transferData: [],
    job_id: "",
    ref_no: ""
  }),
  methods: {
    checkpermission() {
      console.log(1);
      let id = 99;
      if (this.typepage == "IV") {
        id = 8;
      } else if (this.typepage == "SO") {
        id = 1;
      } else if (this.typepage == "DS") {
        id = 4;
      } else if (this.typepage == "QT") {
        id = 0;
      }
      if (!api.checkpermission(1)) {
        console.log(1);
        this.$router.push({ name: "index" });
      }
    },
    removeitemtable(index) {
      // console.log(JSON.stringify(this.dproducts.length))
      // this.searchProductInObject(this.dproducts,index)
      console.log(
        JSON.stringify(this.searchProductInObject(this.dproducts, index))
      );
      this.dproducts.splice(
        this.searchProductInObject(this.dproducts, index),
        1
      );
    },
    checkval(val) {
      console.log(val);
    },
    // searchunticode(val) {
    //   console.log(JSON.stringify(val))
    //   let payload = {
    //     item_code: val.item_code
    //   }
    //   this.isLoading = true
    //   // console.log(payload)
    //   api.searchunitcode(payload,
    //     (result) => {
    //       this.isLoading = false
    //       console.log(JSON.stringify(result.data))
    //       this.unitcode_obj = result.data
    //       this.searchunitcode_m = true
    //     },
    //     (error) => {
    //       this.isLoading = false
    //       console.log(JSON.stringify(error))
    //       alertify.error('Data ข้อมูล Unit code ผิดพลาด');
    //     })
    // },
    selectunitcode_step2(val) {
      console.log(JSON.stringify(val));
      this.searchunitcode_m = false;
      var index = this.findWithAttr(this.dproducts, "item_name", val.item_name);
      if (this.billtype == 0) {
        //สด
        this.dproducts[index].unit_code = val.unit_code;
        this.dproducts[index].price = val.sale_price_1;
        this.dproducts[index].packing_rate_1 = val.rate_1;
        this.dproducts[index].item_amount =
          this.dproducts[index].price * this.dproducts[index].qty -
          this.dproducts[index].discount_word;
      }
      if (this.billtype == 1) {
        //เชื่อ
        this.dproducts[index].unit_code = val.unit_code;
        this.dproducts[index].price = val.sale_price_2;
        this.dproducts[index].packing_rate_1 = val.rate_1;
        this.dproducts[index].item_amount =
          this.dproducts[index].price * this.dproducts[index].qty -
          this.dproducts[index].discount_word;
      }
    },
    // selectwarehousecode(val) {
    //   console.log(JSON.stringify(val))
    //   this.searchwarehousecode_m = false
    //   this.dproducts[this.stock_index].wh_code = val.wh_code
    //   this.dproducts[this.stock_index].shelf_code = val.shelf_code
    //   this.dproducts[this.stock_index].stocklimit = val.qty
    // },
    findWithAttr(array, attr, value) {
      for (var i = 0; i < array.length; i += 1) {
        if (array[i][attr] === value) {
          return i;
        }
      }
      return -1;
    },
    searchAllocate() {
      let payload = {
        keyword: this.Allocate
      };
      console.log(payload);
      this.isLoading = true;
      api.searchAllocate(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.data));
          // console.log(result.data.length)
          if (result.data.length == 0) {
            alertify.error("ไม่มีการจัดสรร");
            return;
          }
          if (result.data.length == 1) {
            this.Allocateid = result.data[0].id;
            this.Allocate = result.data[0].name;
          } else if (result.data.length > 1) {
            this.searchAllocate_m = true;
            this.objAllocate = result.data;
          }
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลการจัดสรรผิดพลาด");
        }
      );
    },
    selectAllocate_step2(val) {
      this.Allocateid = val.id;
      this.Allocate = val.name;
      this.searchAllocate_m = false;
    },
    searchproject_step2() {
      let payload = {
        keyword: this.project
      };
      console.log(payload);
      this.isLoading = true;
      api.searchproject(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.data));
          // console.log(result.data.length)
          if (result.data.length == 0) {
            alertify.error("ไม่มีโครงการนี้");
            return;
          }
          if (result.data.length == 1) {
            this.project = result.data[0].code + " " + result.data[0].name;
            this.idprojectC = result.data[0].id;
          } else if (result.data.length > 1) {
            this.searchproject = true;
            this.objproject = result.data;
          }
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลโครงการผิดพลาด");
        }
      );
    },
    selectproject_step2(val) {
      // console.log(val)
      this.idprojectC = val.id;
      this.project = val.code + " " + val.name;
      this.searchproject = false;
    },
    searchdepart_step2() {
      let payload = {
        keyword: this.department
      };
      this.isLoading = true;
      console.log(payload);
      api.searchdepartment(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.data));
          // console.log(result.data.length)
          if (result.data.length == 0) {
            alertify.error("ไม่มีแผนกนี้");
            return;
          }
          if (result.data.length == 1) {
            this.iddepartment = result.data[0].id;
            this.department = result.data[0].name;
          } else if (result.data.length > 1) {
            this.searchdepart = true;
            this.objdepart = result.data;
          }
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลแผนกผิดพลาด");
        }
      );
    },
    selectdepart_step2(val) {
      this.iddepartment = val.id;
      this.searchdepart = false;
      this.department = val.name;
    },
    selectJobById() {
      this.job_id = this.job_id;
    },
    selectRefNoById() {
      this.ref_no = this.ref_no;
    },
    searchCustContact() {
      let payload = {
        id: this.idcus,
        keyword: this.custcontact
      };
      console.log(payload);
      this.isLoading = true;
      api.searchCustContactByKeyword(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.data));
          if (result.data.length == 0) {
            alertify.error("ไม่พบผู้ติดต่อ");
            return;
          }
          // if (result.data.length == 1) {
          //   this.custcontactid = result.data[0].id
          //   this.custcontact = result.data[0].contact_code + ' ' + result.data[0].contact_name

          // } else if (result.data.length > 1) {
          if (result.data.length > 0) {
            this.searchcustcontact = true;
            this.objCustContact = result.data;
          }
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลผู้ติดต่อผิดพลาด");
        }
      );
    },
    selectCustContact_step2(val) {
      this.custcontactid = val.id;
      this.searchcustcontact = false;
      this.custcontact = val.contact_code + " " + val.contact_name;
    },
    showCustContactById() {
      let payload = {
        id: this.custcontactid
      };
      console.log("payload showCustContactById ", payload);
      if (payload.id == 0) {
        return "";
      }
      api.searchCustContactById(
        payload,
        result => {
          this.custcontactid = result.data[0].id;
          this.custcontact =
            result.data[0].contact_code + " " + result.data[0].contact_name;
          console.log(JSON.stringify(result));
        },
        error => {
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลผิดพลาด");
        }
      );
    },
    isshowdoc_fuc() {
      if (!this.isshowdocument) {
        this.docheight = "72px";
      }
      if (this.isshowdocument) {
        this.docheight = "256px";
      }
    },
    calexpire_Date() {
      var date1 = new Date(this.expiredate_cal);
      this.expiredate_cal =
        date1.getMonth() +
        1 +
        "/" +
        date1.getDate() +
        "/" +
        date1.getFullYear();
      var date2 = new Date();
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      this.expire_date = diffDays;
    },
    calexpiredate() {
      console.log(this.expire_date);
      let date = new Date();
      date.setDate(date.getDate() + parseInt(this.expire_date));
      this.expiredate_cal =
        date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
    },
    calDueDate_date() {
      var date1 = new Date(this.DueDate_date);
      this.DueDate_date =
        date1.getMonth() +
        1 +
        "/" +
        date1.getDate() +
        "/" +
        date1.getFullYear();
      // alert(this.DueDate_date)
      var date2 = new Date();
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      this.Deliver_date = diffDays;
    },
    calDeliverdate() {
      let date = new Date();
      date.setDate(date.getDate() + parseInt(this.Deliver_date));
      this.DueDate_date =
        date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
      console.log(this.DueDate_date);
    },
    calbathordiscount() {
      // alert('dsad')
      console.log(this.percal);
      this.percal = !this.percal;
    },
    // newUser() {
    //   this.$refs.addproduct.$el.focus()
    // },
    searchOnTable() {
      this.searched = searchByName(this.dproducts, this.search);
      console.log(this.searched);
    },
    tests() {
      Swal.fire({
        type: "error",
        title: "Maintenance",
        text: "The Process Of Maintenance "
      });
    },
    setDone(id, index) {
      if (id == "first") {
        console.log(
          "tag",
          this.tablecode,
          this.billtype,
          this.taxtype,
          this.idcus,
          this.dproducts.length
        );
        if (
          this.tablecode == null ||
          this.billtype == null ||
          this.taxtype == null ||
          this.idcus == null ||
          this.dproducts.length <= 0
        ) {
          alert("กรุณากรอกข้อมูลให้ครบ");
          return;
        }
      }
      if (id == "third") {
        this.$router.push("/quolist");
        return;
      }
      //
      this[id] = true;
      this.secondStepError = null;
      //

      //บันทึก

      if (index) {
        this.active = index;
      }
      document.getElementsByClassName("md-content")[0].scrollTop = 0;
    },
    saveDocument() {
      this.isLoading = true;
      let doc_type;
      let tax_type;
      let percent;
      let discount_amount;

      if (this.tablecode == "QT") {
        doc_type = 0;
      } else if (this.tablecode == "BO") {
        doc_type = 1;
      }

      var str = this.salecode;
      var res = str.split("/");
      // console.log(res)
      // var sale_code = res[0]
      // var sale_name = res[1]

      if (this.percal) {
        percent = "%";
        discount_amount =
          this.totalprice -
          (this.totalprice - (this.totalprice * this.caldiscount) / 100);
        //   alert('dsa')
      } else if (!this.percal) {
        percent = "";
        discount_amount = this.caldiscount;
      }
      // console.log(this.datenow_datepicker)
      // console.log(this.docnoid)
      this.showdocno();
      Swal.fire({
        title: "คุณต้องการบันทึกใบราคานี้?",
        text: "ยืนยันการทำใบเสนอราคา!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "ตกลง",
        cancelButtonText: "ยกเลิก"
      }).then(result => {
        if (result.value) {
          for (let i = 0; this.dproducts.length > i; i++) {
            this.dproducts[i].line_number = i + 1;
          }
          let payload = {
            id: parseInt(this.docnoid), // 0 แก้ไข,update ตามไอดี
            branch_id: this.branch_id,
            doc_no: this.docno,
            //norecord
            ar_bill_address: this.ar_bill_address,
            ar_telephone: this.ar_telephone,
            datenow_datepicker: this.datenow_datepicker,
            dif_fee: this.dif_fee,
            //norecord

            doc_type,
            ar_id: this.idcus,
            ar_code: this.searchcus,
            ar_name: this.detailcus,
            sale_id: this.sale_id,
            sale_code: this.salecode,
            sale_name: this.salename,
            bill_type: parseInt(this.billtype),
            tax_type: parseInt(this.taxtype),
            tax_rate: 7,
            depart_code: "",
            ref_no: this.ref_no,
            job_id: this.job_id,
            delivery_id: parseInt(this.ass_id),
            is_confirm: parseInt(this.isConfirm),
            bill_status: 0,
            credit_day: this.bill_credit,
            due_date: this.convermonth_y_m_d(this.DueDate_cal),
            validity: parseInt(this.validity),
            expire_credit: parseInt(this.expire_date),
            expire_date: this.convermonth_y_m_d(this.expiredate_cal),
            delivery_day: parseInt(this.Deliver_date),
            delivery_date: this.convermonth_y_m_d(this.DueDate_date),
            is_condition_send: parseInt(this.is_condition_send),
            my_description: this.infoNotice,
            sum_of_item_amount: this.totalprice,
            discount_word: this.caldiscount + percent,
            discount_amount: parseInt(discount_amount),
            after_discount_amount: this.totalprice - this.caldiscount,
            company_id: parseInt(this.company_id),
            before_tax_amount: this.totalprice,
            assert_status: parseInt(this.answer_cus),
            depart_id: parseInt(this.iddepartment),
            project_id: parseInt(this.idprojectC),
            allocate_id: parseInt(this.Allocateid),
            is_cancel: parseInt(this.isCancel),
            creator_by: this.creator_by,
            contact_id: this.custcontactid,
            subs: this.dproducts,
            discount_select: this.discountSelect,
            price_level: this.customerLevel
          };
          console.log(payload);
          //print zone
          document.getElementsByName("dataquotation")[0].value = JSON.stringify(
            payload
          );

          document.getElementsByName("dataquotation")[1].value = JSON.stringify(
            payload
          );
          console.log(JSON.stringify(payload));
          api.savequotation(
            payload,
            result => {
              if (result.error) {
                // alertify.error('เกิดข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้');
                Swal.fire({
                  type: "error",
                  title: result.error
                });
                this.isLoading = false;
                return;
              }
              if ((result.response = "success")) {
                this.docnoid = result.data.id;
                console.log(this.docnoid);
                setTimeout(() => {
                  this.setDone("second", "third");
                  console.log(result);
                  alertify.success("บันทึกสำเร็จ " + this.docno);
                  this.isLoading = false;
                }, 500);
              }
            },
            error => {
              console.log(JSON.stringify(error));
              //Customerall
              alertify.error("เกิดข้อผิดพลาด");
              this.isLoading = false;
            }
          );
        }
        this.isLoading = false;
      });
    },
    savePDF() {
      const monthNames = [
        "มกราคม",
        "กุมภาพันธ์",
        "มีนาคม",
        "เมษายน",
        "พฤษภาคม",
        "มิถุนายน",
        "กรกฎาคม",
        "สิงหาคม",
        "กันยายน",
        "ตุลาคม",
        "พฤศจิกายน",
        "ธันวาคม"
      ];
      let payload = {
        from: "income",
        from_name: "รายได้ครัวเรือน",
        start:
          monthNames[parseInt(this.trendline.monthbefore)] +
          " " +
          this.trendline.yearbefore,
        end:
          monthNames[parseInt(this.trendline.month)] +
          " " +
          this.trendline.year,
        sumbalnce: this.sumbalnce,
        sumincome: this.sumincome,
        sumdebt: this.sumdebt,
        sumexp: this.sumexp,
        list: this.listreport
      };
      console.log(payload);
      document.getElementsByName("dataquotation")[0].value = JSON.stringify(
        payload
      );
      document.getElementsByName("dataquotation")[1].value = JSON.stringify(
        payload
      );
    },
    PrintPDF() {
      this.isLoading = true;
      console.log(payload);
      api.detailquoall(
        payload,
        result => {
          if (result.data.doc_type == 0) {
            doc_type = "QT";
          } else if (result.data.doc_type == 1) {
            doc_type = "BO";
          }
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("ข้อมูลผิดพลาด detailquoall");
        }
      );
      //inputToPDF
      document.getElementsByName("dataquotation")[0].value = JSON.stringify(
        payload
      );
      document.getElementsByName("dataquotation")[1].value = JSON.stringify(
        payload
      );
      console.log(JSON.stringify(payload));
      this.isLoading = false;
    },
    convermonth_y_m_d(val) {
      if (val == "") {
        return "";
      }
      var date = val;
      var cut = date.split("/");
      var result = cut[2] + "-" + cut[0] + "-" + cut[1];
      return result;
    },
    convertmonth_d_m_y(val) {
      var date = val.substring(0, 10);
      var cut = date.split("-");
      var result = cut[1] + "/" + cut[2] + "/" + cut[0];
      return result;
    },
    fsearchcus() {
      var payload = {
        keyword: this.searchcus
      };
      this.isLoading = true;
      api.Customerall(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.member));
          if (result.member.length == 0) {
            alertify.error("ค้นหาไม่พบ");
            return;
          }
          if (result.member.length == 1) {
            this.idcus = result.member[0].id;
            this.detailcus = result.member[0].name;
            this.searchcus = result.member[0].code;
            this.customerLevel = result.member[0].price_level;
            this.bill_credit = result.member[0].bill_credit;
            var date = new Date();
            date.setDate(date.getDate() + this.bill_credit);
            this.DueDate_cal =
              date.getMonth() +
              1 +
              "/" +
              date.getDate() +
              "/" +
              date.getFullYear();
            this.changePriceType();
            this.selectCustomer = true;
          } else if (result.member.length > 1) {
            this.detailcusall = result.member;
            this.showDialogcus = true;
          }
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          //Customerall
          alertify.error("member ข้อมูลค้นหาลูกค้าผิดพลาด");
          //  alertify.success('Error login');
          // this.cload()
        }
      );
    },
    searchCustomerRT() {
      var payload = {
        keyword: this.searchcus
      };

      api.Customerall(
        payload,
        result => {
          console.log(JSON.stringify(result.member));
          // console.log(result.data.length)
          if (result.member.length == 0) {
            alertify.error("ค้นหาไม่พบ");
            return;
          }
          if (result.member.length == 1) {
            this.idcus = result.member[0].id;
            this.detailcus = result.member[0].name;
            this.searchcus = result.member[0].code;

            this.showDialogcus = false;
          } else if (result.member.length > 1) {
            this.detailcusall = result.member;
            this.showDialogcus = true;
          }
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          //Customerall
          alertify.error("Data ข้อมูลค้นหาลูกค้าผิดพลาด");
          //  alertify.success('Error login');
          // this.cload()
        }
      );
    },
    C_customer(val) {
      console.log(JSON.stringify(val));
      this.idcus = val.id;
      this.searchcus = val.code;
      this.detailcus = val.name;

      this.customerLevel = val.price_level;
      this.bill_credit = val.bill_credit;
      //
      var date = new Date();
      console.log(date);
      date.setDate(date.getDate() + this.bill_credit);
      this.DueDate_cal =
        date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
      console.log(this.DueDate_cal);
      this.changePriceType();
      this.selectCustomer = true;
      this.showDialogcus = false;
    },
    searchProductInObject(objectProduct, arrayPosition) {
      for (var i = 0; i < objectProduct.length; i++) {
        if (objectProduct[i].index == arrayPosition) {
          return i;
        }
      }
      return null;
    },
    showdocno() {
      if (this.docnoid != 0) {
        // alert('หน้าแก้ไข')
        return;
      }
      if (!this.tablecode || !this.billtype) {
        return;
      }

      if (this.dproducts.length > 0) {
        // var test;
        // for (let x = 0; x < this.dproducts.length; x++) {
        //   test +=  this.dproducts[x].bar_code
        // }
        // console.log(test)
      }

      this.disablebilltype = true;
      let payload = {
        branch_id: this.objuser.branch_id,
        table_code: this.tablecode,
        bill_type: parseInt(this.billtype)
      };
      this.isLoading = true;
      console.log(payload);
      api.showdocno(
        payload,
        result => {
          this.isLoading = false;
          if (result.error) {
            this.docno = "ไม่มีข้อมูล";
            return;
          }
          return (this.docno = result);
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          //Customerall
          alertify.error("ข้อมูล ประเภทเสนอราคาเกิดข้อผิดพลาด");
          //  alertify.success('Error login');
          // this.cload()
        }
      );
    },
    mockDocNo() {
      if (!this.tablecode || !this.billtype) {
        return;
      }

      if (this.dproducts.length > 0) {
      }

      this.disablebilltype = true;
      let payload = {
        branch_id: this.objuser.branch_id,
        table_code: this.tablecode,
        bill_type: parseInt(this.billtype)
      };
      this.isLoading = true;
      console.log(payload);
      api.showdocno(
        payload,
        result => {
          this.isLoading = false;
          if (result.error) {
            this.mockdocno = "ไม่มีข้อมูล";
            return;
          }
          this.mockdocno = "";
          for (var i = 0; i < result.length - 4; i++) {
            this.mockdocno += result.charAt(i);
          }
          this.mockdocno += "XXXX";
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          //Customerall
          alertify.error("ข้อมูล ประเภทเสนอราคาเกิดข้อผิดพลาด");
          //  alertify.success('Error login');
          // this.cload()
        }
      );
    },
    // showdetail(val) {
    //   console.log(JSON.stringify(val))
    //   if (this.billtype == 0) {
    //     var datashow = {
    //       item_id: val.id,
    //       item_code: val.item_code,
    //       bar_code: val.bar_code,
    //       item_name: val.item_name,
    //       unit_code: val.unit_code,
    //       qty: 1,
    //       price: val.sale_price_1,
    //       sale_price_1: val.sale_price_1,
    //       sale_price_2: val.sale_price_2,
    //       discount_word: '0',
    //       discount_amount: 0,
    //       item_amount: val.sale_price_1 * 1,
    //       item_description: "",
    //       packing_rate_1: parseInt(val.rate_1),
    //       is_cancel: 0,
    //       wh_code: val.wh_code,
    //       stock_type: val.stock_type,
    //     }
    //     this.dproducts.push(datashow)
    //     console.log(JSON.stringify(this.dproducts))
    //     console.log(JSON.stringify(this.dproducts.length))
    //     //close modal
    //     this.showDialogproduct = false
    //     alertify.success('เพิ่มข้อมูลสินค้า ' + val.item_name);
    //   } else if (this.billtype == 1) {
    //     var datashow = {
    //       item_id: val.id,
    //       item_code: val.item_code,
    //       bar_code: val.bar_code,
    //       item_name: val.item_name,
    //       unit_code: val.unit_code,
    //       qty: 1,
    //       price: val.sale_price_1,
    //       sale_price_1: val.sale_price_1,
    //       sale_price_2: val.sale_price_2,
    //       discount_word: '0',
    //       discount_amount: 0,
    //       item_amount: val.sale_price_1 * 1,
    //       item_description: "",
    //       packing_rate_1: parseInt(val.rate_1),
    //       is_cancel: 0,
    //       wh_code: val.wh_code,
    //       stock_type: val.stock_type,
    //     }
    //     this.dproducts.push(datashow)
    //     //close modal
    //     this.showDialogproduct = false
    //     alertify.success('เพิ่มข้อมูลสินค้า ' + val.item_name);
    //     console.log(JSON.stringify(this.dproducts))
    //     console.log(JSON.stringify(this.dproducts.length))
    //   }
    //   this.keywordproduct = ''

    //   //console.log(datashow)
    // },
    getFocus(id) {
      document.getElementById(id).focus();
    },
    convertmoney(val) {
      // console.log(val)
      var number = numeral(val).format("0,0.00");
      return number;
    },
    convertshowdoc(val) {
      if (val == false) {
        var comment = "ปิด";
        return comment;
      } else if (val == true) {
        var comment = "เปิด";
        return comment;
      }
    },
    showcontent_step2() {
      if (this.docnoid == 0) {
        this.salecode = this.objuser.sale_code;
        this.salename = this.objuser.username;
        this.salemock = this.objuser.sale_code + "/" + this.objuser.username;
        this.selectSale = true;
      }
    },
    searchsale_step2() {
      let payload = {
        keyword: this.salecode
      };
      this.isLoading = true;
      console.log(payload);
      api.searchcus(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.data));
          // console.log(result.data.length)
          if (result.data.length == 0) {
            alertify.error("ไม่มีพนักงานคนนี้");
            return;
          }
          if (result.data.length == 1) {
            this.salecode = result.data[0].sale_code;
            this.salename = result.data[0].sale_name;
            this.selectSale = true;
            this.salemock = this.salecode + "/" + this.salename;
          } else if (result.data.length > 1) {
            this.searchsale = true;
            this.searchsaleobj = result.data;
          }
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลค้นหาลูกค้าผิดพลาด");
        }
      );
    },
    selectcus_step2(val) {
      console.log(JSON.stringify(val));
      console.log(JSON.stringify(this.objuser));
      this.sale_id = val.employee_id;
      this.salecode = val.sale_code;
      this.salename = val.sale_name;
      this.salemock = this.salecode + "/" + this.salename;
      this.selectSale = true;
      this.searchsale = false;
    },
    focussearchcus() {
      this.salecode = "";
      this.$refs.codesale.$el.focus();
    },
    showedit() {
      if (this.docnoid == 0) {
        // alert('หนักหลัก')
        return;
      }
      // alert('แก้ไข')
      else {
        let payload = {
          id: parseInt(this.docnoid)
        };
        this.isLoading = true;
        console.log(JSON.stringify(payload));
        api.detailquoall(
          payload,
          result => {
            this.isLoading = false;
            let doc_type;
            let tax_type;
            if (result.data.doc_type == 0) {
              doc_type = "QT";
            } else if (result.data.doc_type == 1) {
              doc_type = "BO";
            }
            // this.dproducts = []
            this.infoNotice = result.data.my_description;
            this.disablebilltype = true;
            this.tablecode = doc_type;
            this.billtype = result.data.bill_type;
            this.sale_id = result.data.sale_id;
            this.ar_bill_address = result.data.ar_bill_address;
            this.ar_telephone = result.data.ar_telephone;
            this.docno = result.data.doc_no;
            this.taxtype = result.data.tax_type;
            this.customerLevel = result.data.price_level;
            this.datenow_datepicker = result.data.doc_date;
            this.idcus = result.data.ar_id;
            this.searchcus = result.data.ar_code;
            this.detailcus = result.data.ar_name;
            this.isConfirm = result.data.is_confirm;
            this.isCancel = result.data.is_cancel;
            this.idprojectC = result.data.project_id;
            this.depart_id = result.data.depart_id;
            this.Allocateid = result.data.allocate_id;
            this.ref_no = result.data.ref_no;
            this.job_id = result.data.job_id;
            this.ass_id = result.data.delivery_id;
            var datasubs = result.data.subs;
            let data;
            for (let x = 0; x < datasubs.length; x++) {
              console.log(datasubs[0].price);
              console.log(datasubs[x].qty);
              data = {
                line_number: datasubs[x].line_number,
                item_id: datasubs[x].id,
                item_code: datasubs[x].item_code,
                bar_code: datasubs[x].bar_code,
                item_name: datasubs[x].item_name,
                unit_code: datasubs[x].unit_code,
                qty: datasubs[x].qty,
                price: datasubs[x].price,
                discount_word: datasubs[x].discount_word,
                discount_amount: datasubs[x].discount_amount,
                item_amount: datasubs[x].item_amount,
                item_description: datasubs[x].item_description,
                packing_rate_1: datasubs[x].packing_rate_1,
                is_cancel: datasubs[x].is_cancel,
                wh_code: datasubs[x].wh_code
              };
              console.log(JSON.stringify(data));
              console.log(data.item_amount);
              this.dproducts.push(data);
              console.log("dproducts :" + this.dproducts.length);
            }
            console.log(JSON.stringify(this.dproducts));
            this.salecode = result.data.sale_code;
            this.salename = result.data.sale_name;
            this.validity = result.data.validity;
            this.expire_date = result.data.expire_credit;
            this.caldiscount = result.data.discount_amount;
            this.answer_cus = result.data.assert_status;
            this.Deliver_date = result.data.delivery_day;
            this.bill_credit = result.data.credit_day;
            this.is_condition_send = result.data.is_condition_send;
            this.expiredate_cal = this.convertmonth_d_m_y(
              result.data.expire_date
            );
            // console.log(this.expiredate_cal)
            this.DueDate_date = this.convertmonth_d_m_y(
              result.data.delivery_date
            );
            this.DueDate_cal = this.convertmonth_d_m_y(result.data.due_date);
            // console.log(this.DueDate_cal)
            this.my_description = result.data.my_description;
            this.custcontactid = result.data.contact_id;
            //  console.log(this.dproducts)
            console.log(JSON.stringify(result.data));
            console.log(JSON.stringify(result.data.subs));
            // console.log(result.data.bill_type)
            // console.log(JSON.stringify(result.data.is_confirm))
            // console.log("idcus :"+this.idcus)
            // console.log("taxtype :"+this.taxtype)
            // console.log("billtype :"+this.billtype)
            // console.log("tableccode :"+this.tablecode)
            console.log(datasubs);
            this.salemock = this.salecode + "/" + this.salename;
            this.selectSale = true;
            this.showDepartment();
            this.showprojectById();
            this.showAllocateById();
            this.showCustContactById();
          },
          error => {
            this.isLoading = false;
            console.log(JSON.stringify(error));
            alertify.error("ข้อมูลผิดพลาด detailquoall");
          }
        );
      }
    },
    convertmonth_preview(val) {
      if (val.length === undefined) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
          dd = "0" + dd;
        }
        if (mm < 10) {
          mm = "0" + mm;
        }
        today = yyyy + "-" + mm + "-" + dd;
        return today;
      } else if (val.length != undefined) {
        return val.substring(0, 10);
      }
    },
    getItems(keyword, s_type, i) {
      // var price_type;
      api.findproductByKeyword(
        { keyword: keyword, sale_type: s_type },
        result => {
          if (this.customerLevel == 1 || this.customerLevel == "") {
            this.dproducts[i].price = result.data[0].sale_price_1;
            this.dproducts[i].item_amount =
              (result.data[0].sale_price_1 - this.dproducts[i].discount_word) *
              this.dproducts[i].qty;
          } else if (this.customerLevel == 2) {
            this.dproducts[i].price = result.data[0].sale_price_2;
            this.dproducts[i].item_amount =
              (result.data[0].sale_price_2 - this.dproducts[i].discount_word) *
              this.dproducts[i].qty;
          }
        },
        error => {
          // alertify.error(
          //     "ของในคลังหมด"
          //   );
        }
      );
    },
    changePriceType() {
      if (this.docnoid == 0) {
        for (var i = 0; i < this.dproducts.length; i++) {
          console.log(JSON.stringify(this.dproducts[i]));
          if (this.billtype == 0) {
            this.getItems(this.dproducts[i].item_code, 0, i);
          }
          if (this.billtype == 1) {
            this.getItems(this.dproducts[i].item_code, 1, i);
          }
          console.log(JSON.stringify("sdfsdfsdf " + this.dproducts[i].price));
          console.log(JSON.stringify(this.dproducts[i]));
        }
      }
    },
    changevaluetest() {
      this.tablecode = "QT";
    },
    changevaluetest2() {
      this.tablecode = "BO";
    },
    // findstock(val, index) {

    //   console.log(index)

    //   for (let i = 0; i < this.stockall.length; i++) {
    //     document.getElementsByClassName('hover' + index)[i].style.visibility = 'visible';
    //     document.getElementsByClassName('hover' + index)[i].style.height = '20px';
    //   }
    //   return

    //   this.namestock = ''
    //   this.stockobj = []
    //   console.log(val.item_code)
    //   var payload = {
    //     item_code: val.item_code
    //   }
    //   this.isLoading = true
    //   api.searchunitcode(payload,
    //     (result) => {
    //       this.isLoading = false
    //       console.log(result)
    //       console.log(JSON.stringify(result.data))
    //       console.log(JSON.stringify(result.data.stock))
    //       this.namestock = result.data[0].item_name
    //       this.stockobj = result.data[0].stk_location
    //       console.log(result.data[0].stk_location.length)
    //     },
    //     (error) => {
    //       this.isLoading = false
    //       console.log(JSON.stringify(error))
    //       alertify.error('Data ข้อมูล ค้นหาคลัง ผิดพลาด');
    //     })
    //   // alert('ทดสอบ')
    // },
    confirmDoc() {
      Swal.fire({
        title: "ยืนยันการอนุมัติ?",
        text: "เมื่อยืนยันระบบจะทำการอนุมัติบิลนี้!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes"
      }).then(result => {
        if (result.value) {
          let payload = {
            id: parseInt(this.docnoid, 10),
            confirm_by: JSON.parse(sessionStorage.Datauser).usercode,
            assert_status: this.answer_cus
          };
          console.log(JSON.stringify(payload));
          // this.isLoading = true;
          api.confirmQuotation(
            payload,
            result => {
              alertify.success("confirm เอกสารแล้ว");
              // this.isLoading = false;
              console.log(JSON.stringify(this.isConfirm));
              console.log(JSON.stringify(this.isCancel));
              console.log(JSON.stringify(this.answer_cus));
              console.log(JSON.stringify(this.permission.is_update));
              setTimeout(() => {
                location.reload();
              }, 1000);
            },
            error => {
              // this.isLoading = false
              console.log(JSON.stringify(error));
              alertify.error("ข้อมูล ผิดพลาด");
            }
          );
        }
      });
    },
    cancelDoc() {
      Swal.fire({
        title: "ยืนยันการยกเลิก?",
        text: "เมื่อยืนยันระบบจะทำการยกเลิกบิลนี้!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes"
      }).then(result => {
        if (result.value) {
          let payload = {
            id: parseInt(this.docnoid, 10),
            cancel_by: JSON.parse(sessionStorage.Datauser).usercode
          };
          console.log(JSON.stringify(payload));
          api.cancelQuotation(
            payload,
            result => {
              alertify.success("cancel เอกสารแล้ว");
              setTimeout(() => {
                location.reload();
              }, 1000);
            },
            error => {
              console.log(JSON.stringify(error));
              alertify.error("Data ข้อมูล ผิดพลาด");
            }
          );
        }
      });
    },
    callQTtoSO() {
      Swal.fire({
        title: "ยืนยันการนำไปทำใบสั่งขาย?",
        text: "เมื่อยืนยันระบบนำไปทำใบสั่งขาย!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes"
      }).then(result => {
        if (result.value) {
          let payload = {
            id: parseInt(this.docnoid)
          };
          let sodocno;
          console.log("callQTtoSO", payload);
          api.transferQTtoSO(
            payload,
            result => {
              if (result.error) {
                Swal.fire({
                  type: "error",
                  title: result.error
                });
                // alertify.error('เอกสารโอนเป็นใบสั่งขายไปแล้ว')
              }
              console.log(JSON.stringify(result));
              sodocno = { id: result.data.id };
              console.log(JSON.stringify(sodocno));
              setTimeout(() => {
                this.$router.push({
                  name: "saleorder",
                  params: { id: sodocno.id }
                });
              }, 1000);
            },
            error => {
              console.log(JSON.stringify(error));
              alertify.error("เกิดข้อผิดพลาด ไม่สามารถโอนใบเสนอราคาได้");
            }
          );
          return;
        }
      });
    },
    showDepartment() {
      let payload = {
        id: this.depart_id
      };
      console.log("payload showDepartment", payload);
      if (payload.id == 0) {
        return "";
      }
      api.searchdepartmentById(
        payload,
        result => {
          this.iddepartment = result.data.id;
          this.departmentCode = result.data.code;
          this.department = result.data.name;
          console.log(JSON.stringify(result));
        },
        error => {
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลผิดพลาด");
        }
      );
    },
    showprojectById() {
      let payload = {
        id: this.idprojectC
      };
      console.log("payload showprojectById", payload);
      if (payload.id == 0) {
        return "";
      }
      api.searchprojectById(
        payload,
        result => {
          this.idprojectC = result.data.id;
          this.project = result.data.code + " " + result.data.name;
          console.log(JSON.stringify(result));
        },
        error => {
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลผิดพลาด");
        }
      );
    },
    showAllocateById() {
      let payload = {
        id: this.Allocateid
      };
      console.log("payload showAllocateById ", payload);
      if (payload.id == 0) {
        return "";
      }
      api.searchAllocateById(
        payload,
        result => {
          this.Allocateid = result.data.id;
          this.Allocate = result.data.code + " " + result.data.name;
          console.log(JSON.stringify(result));
        },
        error => {
          console.log(JSON.stringify(error));
          alertify.error("Data ข้อมูลผิดพลาด");
        }
      );
    },
    stepOne: function(event) {
      this.setDone("first", "second");
    }
  },
  created() {
    console.log(JSON.stringify(this.searched));
    this.searched = this.dproducts;
    console.log(JSON.stringify(this.searched));
  },
  computed: {
    keymap() {
      return {
        "ctrl+shift+1": this.changevaluetest,
        "ctrl+shift+2": this.changevaluetest2
      };
    },

    totalprice() {
      return this.dproducts.reduce(function(sum, item) {
        return sum + item.item_amount;
      }, 0);
    },
    dif_fee() {
      if (this.taxtype == 0 || this.taxtype == 1) {
        if (!this.percal) {
          return (
            this.totalprice -
            this.caldiscount -
            ((this.totalprice - this.caldiscount) * 100) / 107
          );
        } else if (this.percal) {
          let percent =
            this.totalprice - (this.totalprice * this.caldiscount) / 100;
          console.log(percent);
          return percent - (percent * 100) / 107;
        }
      }
      if (this.taxtype == 2) {
        return 0;
      }
    },
    cal_totalprice() {
      if (this.taxtype == 1) {
        if (!this.percal) {
          return this.totalprice - this.caldiscount;
        }
        if (this.percal) {
          return this.totalprice - (this.totalprice * this.caldiscount) / 100;
        }
      }
      if (this.taxtype == 0) {
        if (!this.percal) {
          return this.totalprice + this.dif_fee;
        }
        if (this.percal) {
          return this.totalprice + this.dif_fee;
        }
      }
      if (this.taxtype == 2) {
        if (!this.percal) {
          return this.totalprice;
        }
        if (this.percal) {
          return this.totalprice;
        }
      }
    },
    firstDayOfAWeek: {
      get() {
        return this.$material.locale.firstDayOfAWeek;
      },
      set(val) {
        this.$material.locale.firstDayOfAWeek = val;
      }
    }
  },
  mounted() {
    this.checkpermission();
    console.log(JSON.stringify(this.objuser.username));
    this.docnoid = this.$route.params.id;
    // console.log(JSON.stringify(JSON.parse(sessionStorage.Datauser)))
    // console.log(JSON.stringify(this.searched))
    // console.log(JSON.stringify(this.permission))
    this.showedit();
    this.creator_by = this.objuser.usercode;
    this.branch_id = this.objuser.branch_id;
    this.creator_by = this.objuser.usercode;
    this.branch_id = this.objuser.branch_id;
    this.showcontent_step2();
    // console.log(this.objuser)
    this.calexpiredate();
    this.calDeliverdate();
    // this.changePriceType()
    this.mockDocNo();
  }
};
