import api from "../service/service.js";
import facebookLogin from "facebook-login-vuejs";
import auth from "../service/auth.js";
import vs from "../service/version.json";
import Swal from "sweetalert2";
export default {
  name: "login",
  components: {
    facebookLogin
  },
  data() {
    return {
      loading: false,
      login: {
        id: "",
        password: "",
        value: "9"
      },
      loggedIn: auth.loggedIn(),
      version: vs.major + "." + vs.minor + "." + vs.patch,
      dateversion: vs.date
    };
  },
  created() {
    auth.onChange = loggedIn => {
      this.loggedIn = loggedIn;
    };
  },
  watch: {
    loggedIn: function() {
      if (this.loggedIn) {
        this.$router.push({ name: "index" });
        return;
      }
    }
  },

  methods: {
    convert_vs(val) {
      var data = [];
      var len = 3;
      if (val.length > 3) {
        len = val.length;
      }
      var vallen = val.length;
      for (var i = 0; i < len; i++) {
        var v = val.split("");
        if (len - i === vallen) {
          var ss = len - i;
          if (ss === vallen) {
            if (val.length === 1) {
              data.push(val);
            } else {
              if (len > val.length) {
                data.push(v[i - 1]);
              } else {
                data.push(v[i]);
              }
            }
          } else {
            data.push(v[i - 1]);
          }
          vallen--;
        } else {
          data.push("0");
        }
      }
      var versions = "";
      for (var l = 0; l < data.length; l++) {
        versions += data[l];
        if (l < data.length - 1) {
          versions += ".";
        }
      }
      return versions;
    },
    // auth() {
    //   this.loading = true;
    //   let { id, password } = this.login;
    //   api.signin(
    //     id,
    //     password,
    //     result => {
    //       // this.cload()
    //       sessionStorage.test = this.login.password;
    //       //console.log(sessionStorage.test)
    //       if (result.status == "invalid connection") {
    //         alertify.error("เกิดข้อผิดพลาด");
    //         return;
    //       }
    //       if (result.status === "success") {
    //         //console.log(JSON.stringify(result.data))
    //         //console.log(result.data)
    //         sessionStorage.Datauser = JSON.stringify(result.data);
    //         // var userid =
    //         // sessionStorage.branchid = result.data.branch_id
    //         sessionStorage.userid = result.data.id;
    //         sessionStorage.company_id = result.data.company_id;
    //         // sessionStorage.user = result.data.usercode

    //         setTimeout(() => {
    //           this.$router.push({ name: "index", params: { id: 0 } });
    //           alertify.success("Login Success");
    //           this.loading = false;
    //         }, 1000);
    //         // return;
    //         // this.$router.push("/index");
    //       } else {
    //         alertify.error("เกิดข้อผิดพลาด");
    //       }
    //     },
    //     error => {
    //       this.login = {};

    //       this.loading = false;
    //       setTimeout(() => {
    //         switch (error.response.data.message) {
    //           case "No Content = UserName Not Active":
    //             alertify.error("ผู้ใช้ถูกระงับการใช้งาน");
    //             break;
    //           case "No Content = UserName or Password Invalid":
    //             alertify.error(
    //               "ผู้ใช้ไม่มีสิทธิในแอพนี้หรือรหัสผ่านไม่ถูกต้อง"
    //             );
    //             break;
    //           default:
    //             alertify.error("เกิดข้อผิดพลาดนะ");
    //             this.loading = false;
    //             break;
    //         }
    //       }, 10);

    //       // this.cload()
    //     }
    //   );
    // },
    signin() {
      // this.$router.push({ name: "SelectWh" });
      auth.login(this.login.id, this.login.password, loggedIn => {
        if (!loggedIn) {
          this.error = true;
        } else {
          console.log(loggedIn);
          this.$router.replace(this.$route.query.redirect || "/index");
        }
      });
    },
    testgetheader() {
      var req = new XMLHttpRequest();
      req.open("post", document.location, false);
      req.send(null);
      var headers = req.getAllResponseHeaders().toLowerCase();
      console.log(headers);
 
    },
    getUserData(val) {
      console.log(val);
    },
    logout(val) {
      console.log(val);
    },
    onLogout(val) {
      console.log(val);
    },
    focuspass() {
      this.$refs.pass.$el.focus();
    }
  },

  mounted() {
    this.testgetheader();
    auth.onChange = loggedIn => {
      this.loggedIn = loggedIn;
    };

    if (this.loggedIn) {
      this.$router.push({ name: "index" });
    }
  }
};
