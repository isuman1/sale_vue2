import api from "../service/service.js";
import Loading from "vue-loading-overlay";
import Swal from 'sweetalert2';
export default {
  name: "setting",
  data() {
    return {
      objuser: JSON.parse(sessionStorage.Datauser),
      confirmDialog: false,
      isLoading: false,
      star: true,
      msg: "",
      Search: "",
      setting_saleType: "",
      setting_feeType: "",
      setting_taxRate: 7.0,
      id: 0,
      company_id: 0,
      branch_id: 0,
      tax_type: 0,
      tax_rate: 7,
      logo_path: "",
      depart_id: 1,
      def_sale_wh_id: 0,
      def_sale_shelf_id: 0,
      def_buy_wh_id: 0,
      def_buy_shelf_id: 0,
      stock_status: 0,
      sale_tax_type: 0,
      buy_tax_type: 0,
      sale_bill_type: 0,
      buy_bill_type: 0,
      use_address: 0,
      pos_def_cust_id: 0,
      pos_def_stock: 0,
      def_cust_id: 0,
      point_kpi: 0,
      create_by: "",
      create_time: "",
      edit_by: "",
      edit_time: "",
      Com_id: 0,
      Com_com_sys_id: 0,
      Com_company_name: "",
      Com_name_eng: "",
      Com_address: "",
      Com_telephone: "",
      Com_fax: "",
      Com_tax_number: "",
      Com_email: "",
      Com_web_site: "",
      Com_active_status: 0,
      branch_id: 0,
      branch_company_id: 0,
      branch_branch_name: "",
      branch_address: "",
      branch_telephone: "",
      branch_fax: "",
      branch_tax_no: "",
    };
  },
  components: {
    Loading,
  },
  methods: {
    showedit() {
      console.log("id = ",this.id)
      let payload = {
        id: parseInt(this.id)
      };
      this.isLoading = true;
      console.log(payload);
      api.showSetting(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.data));
          this.id = result.data[0].id
          this.company_id = result.data[0].company_id
          this.branch_id = result.data[0].branch_id
          this.tax_type = result.data[0].tax_type
          this.tax_rate = result.data[0].tax_rate
          this.logo_path = result.data[0].logo_path
          this.depart_id = result.data[0].depart_id
          this.def_sale_wh_id = result.data[0].def_sale_wh_id
          this.def_sale_shelf_id = result.data[0].def_sale_shelf_id
          this.def_buy_wh_id = result.data[0].def_buy_wh_id
          this.def_buy_shelf_id = result.data[0].def_buy_shelf_id
          this.stock_status = result.data[0].stock_status
          this.sale_tax_type = result.data[0].sale_tax_type
          this.buy_tax_type = result.data[0].buy_tax_type
          this.sale_bill_type = result.data[0].sale_bill_type
          this.buy_bill_type = result.data[0].buy_bill_type
          this.use_address = result.data[0].use_address
          this.pos_def_cust_id = result.data[0].pos_def_cust_id
          this.pos_def_stock = result.data[0].pos_def_stock
          this.def_cust_id = result.data[0].def_cust_id
          this.point_kpi = result.data[0].point_kpi
          // this.create_by = result.data[0].create_by
          // this.create_time = result.data[0].create_time
          // this.edit_by = result.data[0].edit_by
          // this.edit_time = result.data[0].edit_time
          this.CompanyDetail()
          this.barnchDetail()
        },
        error => {
          this.isLoading = false
          console.log(JSON.stringify(error))
          alertify.error('ข้อมูลเกิดข้อผิดพลาด');
      });
    },
    setdone(id){
      console.log("id = ",this.id)
      Swal.fire({
        title: 'คุณต้องการบันทึกใบราคานี้?',
        text: "ยืนยันการทำใบเสนอราคา!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก'
      }).then((result) => {
        if (result.value) {
      let payload = {
        id: parseInt(this.id),
        company_id: parseInt(this.company_id),
        branch_id: parseInt(this.branch_id),
        tax_type: parseInt(this.tax_type),
        tax_rate: parseInt(this.tax_rate),
        logo_path: this.logo_path,
        depart_id: parseInt(this.depart_id),
        def_sale_wh_id: parseInt(this.def_sale_wh_id),
        def_sale_shelf_id: parseInt(this.def_sale_shelf_id),
        def_buy_wh_id: parseInt(this.def_buy_wh_id),
        def_buy_shelf_id: parseInt(this.def_buy_shelf_id),
        stock_status: parseInt(this.stock_status),
        sale_tax_type: parseInt(this.sale_tax_type),
        buy_tax_type: parseInt(this.buy_tax_type),
        sale_bill_type: parseInt(this.sale_bill_type),
        buy_bill_type: parseInt(this.buy_bill_type),
        use_address: parseInt(this.use_address),
        pos_def_cust_id: parseInt(this.pos_def_cust_id),
        pos_def_stock: parseInt(this.pos_def_stock),
        def_cust_id: parseInt(this.def_cust_id),
        point_kpi: parseInt(this.point_kpi),
        create_by: this.objuser.username
      };
      api.createSetting(payload,
        result => {
          console.log(result)
          alertify.success('บันทึกสำเร็จ ');
      },
        error => {
          console.log(JSON.stringify(error))
          alertify.error('เกิดข้อผิดพลาด');
      })
      } this.isLoading = false
    })
    },
    CompanyDetail(){
      console.log("this.company_id:",this.company_id)
      let payload = {
        id: parseInt(this.company_id),
      };
      api.searchCompanyDetail(payload,
        result => {
          console.log(result)
            this.Com_id = result.data[0].id
            this.Com_com_sys_id = result.data[0].com_sys_id
            this.Com_company_name = result.data[0].company_name
            this.Com_name_eng = result.data[0].name_eng
            this.Com_address = result.data[0].address
            this.Com_telephone = result.data[0].telephone
            this.Com_fax = result.data[0].fax
            this.Com_tax_number = result.data[0].tax_number
            this.Com_email = result.data[0].email
            this.Com_web_site = result.data[0].web_site
            this.Com_active_status = result.data[0].active_status
      },
        error => {
          console.log(JSON.stringify(error))
          alertify.error('เกิดข้อผิดพลาด');
      })
    },
    barnchDetail(){
      console.log("this.branch_id:",this.branch_id)
      let payload = {
        id: parseInt(this.branch_id),
      };
      api.searchBranchDetail(payload,
        result => {
          console.log(result)
            this.branch_id = result.data[0].id
            this.branch_company_id = result.data[0].company_id
            this.branch_branch_name = result.data[0].branch_name
            this.branch_address = result.data[0].address
            this.branch_telephone = result.data[0].telephone
            this.branch_fax = result.data[0].fax
            this.branch_tax_no = result.data[0].tax_no
      },
        error => {
          console.log(JSON.stringify(error))
          alertify.error('เกิดข้อผิดพลาด');
      })
    },
  },
  mounted() {
    this.id = this.$route.params.id;
    this.showedit();
  },
};
