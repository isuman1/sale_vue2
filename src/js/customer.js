import Loading from "vue-loading-overlay";
import "vue-loading-overlay/dist/vue-loading.css";
import Datepicker from "vuejs-datepicker";
import ThailandAutoComplete from 'vue-thailand-address-autocomplete'
import * as lang from "vuejs-datepicker/src/locale";
import Swal from "sweetalert2";
import api from "../service/service.js";
import JQuery from "jquery";
let $ = JQuery;

export default {
  name: "member",
  data: () => ({
    datenow_datepicker: Date.now(),
    language: "th",
    languages: lang,
    showDialogcus: false,
    showContact: false,
    contactAdd: false,
    customerCode: "",
    searchcus: "",
    detailcusall: [],
    detailcus: "",
    active: "first",
    isLoading: false,
    fullPage: true,
    telephone: "",
    email: "",
    address: "",
    fax: "",
    debt_amount: 0,
    debt_limit: 0,
    company_id: 0,
    bill_credit: 0,
    create_by: "",
    member_id: "",
    tax_no: "",
    branch_id: 0,
    idcus: 0,
    point_balance: 0,
    deliveryAdd: false,
    deliveryList: false,
    ass_district: "",
    ass_district_sub: "",
    ass_country: "",
    ass_province: "",
    ass_post_code: "",
    ass_landmarks: "",
    ass_telephone: "",
    ass_address: "",
    objuser: JSON.parse(sessionStorage.Datauser),
    objAddress:[],
    objContact: "",
    con_contact_code: "",
    con_contact_name: "",
    con_address: "",
    con_telephone: "",
    con_email: "",
    con_line_id: "",
    con_line_number: "",
    address_number: '',
    address_area: '',
    district: '',
    amphoe: '',
    province: '',
    zipcode: '',
    insertAdderss: false,
    swarp: 1,
    person_type: 1,
    id_card_no: '',
    price_level: 1,
    tax_type: 1,
    default_tax_rate: 7,
    parent_code:'',
    type_code:'',
    group_code:'',
    group_of_debt:'',
    path_picture:'',
    credit_men_code:'',
    press_men_code:'',
    keep_money_men_code:'',
    cond_pay_code:'',
    group_credit_code:'',
  }),
  components: {
    Datepicker,
    Loading,
    ThailandAutoComplete
  },
  methods: {
    test() {
      Swal.fire({
        type: "error",
        title: "Maintenance",
        text: "The Process Of Maintenance "
      });
    },
    selectWarp(val){
      console.log(val)
      switch (val) {
        case 1 :
          this.swarp = 1
        break
        case 2 :
          this.swarp = 2
        break
      }
    },
    select (address) {
      this.district = address.district
      this.amphoe = address.amphoe
      this.province = address.province
      this.zipcode = address.zipcode
    },
    insertToText () {
      this.address = this.address_area+"  "+"ตำบล."+this.district+"  อำเภอ."+this.district+"  จังหวัด."+this.province+"  "+this.zipcode
      // this.district = this.district
      // this.amphoe = this.amphoe
      // this.province = this.province
      // this.zipcode = this.zipcode
      this.insertAdderss = false
    },
    fsearchcus() {
      var payload = {
        keyword: this.searchcus
      };
      this.isLoading = true;
      api.Customerall(
        payload,
        result => {
          this.isLoading = false;
          console.log(JSON.stringify(result.member));
          if (result.member.length == 0) {
            alertify.error("ไม่มีข้อมูลลูกค้านี้");
            return;
          }
          // if (result.member.length == 1) {
          //   this.idcus = result.member[0].id;
          //   this.detailcus = result.member[0].name;
          //   this.searchcus = result.member[0].code;
          //   this.selectCustomer = true;
          //   this.CustomerByID()
          // } else if (result.member.length > 1) {
          //   this.detailcusall = result.member;
          //   this.showDialogcus = true;
          // }
          this.detailcusall = result.member;
          this.showDialogcus = true;
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("member ข้อมูลค้นหาลูกค้าผิดพลาด");
        }
      );
    },
    CustomerByID(val) {
      if (this.idcus != 0) {
      console.log(JSON.stringify(val));
      var payload = {
        id: parseInt(val)
      };
      this.isLoading = true;
      api.findCustomerID(
        payload,
        result => {
          console.log(JSON.stringify(result));
          this.idcus = result.member.id;
          this.searchcus = result.member.code;
          this.detailcus = result.member.name;
          this.address = result.member.address;
          this.telephone = result.member.telephone;
          this.bill_credit = result.member.bill_credit;
          this.email = result.member.email;
          this.credit_amount = result.member.credit_amount;
          this.company_id = result.member.company_id;
          this.create_by = result.member.create_by;
          this.fax = result.member.fax;
          this.tax_no = result.member.tax_no;
          this.debt_amount = result.member.debt_amount;
          this.debt_limit = result.member.debt_limit;
          this.member_id = result.member.member_id;
          this.point_balance = result.member.point_balance;
          this.isLoading = false;
          this.showDialogcus = false;
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("member ข้อมูลค้นหาลูกค้าผิดพลาด");
        }
      );
      }
    },
    ConfirmCustomer() {
      Swal.fire({
        title: "ยืนยันการอนุมัติ?",
        text: "เมื่อยืนยันระบบจะทำการอนุมัติบิลนี้!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes"
      }).then(result => {
        if (result.value) {
          var payload = {
            id: this.idcus,
            code: this.searchcus,
            name: this.detailcus,
            address: this.address,
            telephone: this.telephone,
            fax: this.fax,
            tax_no: this.tax_no,
            bill_credit: parseInt(this.bill_credit),
            debt_amount: parseInt(this.debt_amount),
            debt_limit: parseInt(this.debt_limit),
            member_id: this.member_id,
            point_balance: parseInt(this.point_balance),
            active_status: this.active_status,
            email: this.email,
            create_by: this.objuser.sale_code,
            credit_amount: this.credit_amount,
            company_id: parseInt(this.company_id),
            branch_id: this.branch_id,
            person_type: parseInt(this.person_type),
            id_card_no: this.id_card_no,
            price_level: parseInt(this.price_level),
            tax_type: parseInt(this.tax_type),
            default_tax_rate: parseInt(this.default_tax_rate),
            parent_code: this.parent_code,
            type_code: this.type_code,
            group_code: this.group_code,
            group_of_debt: this.group_of_debt,
            path_picture: this.path_picture,
            credit_men_code: this.credit_men_code,
            press_men_code: this.press_men_code,
            keep_money_men_code: this.keep_money_men_code,
            cond_pay_code: this.cond_pay_code,
            group_credit_code: this.group_credit_code,
          };
          this.isLoading = true;
          console.log(payload);
          if (this.idcus == 0) {
            api.CustomerNew(
              payload,
              result => {
                this.isLoading = false;
                console.log(result);
                if ((result.result = "insert success")) {
                  Swal.fire({
                    position: "top-end",
                    type: "success",
                    title: result.result,
                    showConfirmButton: false,
                    timer: 1500
                  });
                } else {
                  Swal.fire({
                    type: "error",
                    title: "Something went wrong!",
                    text: result
                  });
                }
              },
              error => {
                this.isLoading = false;
                console.log(JSON.stringify(error));
                alertify.error("member ข้อมูลค้นหาลูกค้าผิดพลาด");
              }
            );
          } else {
            api.CustomerUpdate(
              payload,
              result => {
                this.isLoading = false;
                console.log(result);
                if ((result.result = "update success")) {
                  Swal.fire({
                    position: "top-end",
                    type: "success",
                    title: result.result,
                    showConfirmButton: false,
                    timer: 1500
                  });
                } else {
                  Swal.fire({
                    type: "error",
                    title: "Something went wrong!",
                    text: result
                  });
                }
              },
              error => {
                this.isLoading = false;
                console.log(JSON.stringify(error));
                alertify.error("member ข้อมูลค้นหาลูกค้าผิดพลาด");
              }
            );
          }
        }
      });
    },
    createAddressSO() {
      Swal.fire({
        title: "ยืนยันการอนุมัติ?",
        text: "เมื่อยืนยันระบบจะทำการอนุมัติบิลนี้!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes"
      }).then(result => {
        if(result.value){
        let payload = {
          id: 0,
          company_id: 1,
          branch_id: 1,
          ar_id: this.idcus,
          code: this.ass_code,
          address: this.ass_address,
          district_sub: this.ass_district_sub,
          district: this.ass_district,
          province: this.ass_province,
          post_code: this.ass_post_code,
          country: this.ass_country,
          landmarks: this.ass_landmarks,
          telephone: this.ass_telephone,
          map_location_x: this.ass_map_location_y,
          map_location_y: this.ass_map_location_y
        };
        console.log(payload);
        this.isLoading = true;
        api.createAddress(
          payload,
          result => {
            console.log(result);
            this.isLoading = false;
            this.deliveryAdd = false
          },
          error => {
            this.isLoading = false;
            console.log(JSON.stringify(error));
            alertify.error("Data ข้อมูลแผนกผิดพลาด");
          }
        );
      }});
    },
    openAddAddress(){
    if(this.idcus == 0){
          Swal.fire({
          title: "คุณไม่ได้เลือกลูกค้า!",
          text: "กรุณาเลือกลูกค้า",
          type: "warning",
          showCancelButton: false,
          confirmButtonColor: "#3085d6",
          confirmButtonText: "ตกลง"
        })
      }else{
        this.deliveryAdd = true
      }
    },
    SearchKeywordAddress(){
      let payload = {
        keyword: String(this.idcus)
      }
      console.log(payload)
      this.isLoading = true
      api.SearchAddressByKeyword(payload,
        (result)=>{
          console.log(result)
          this.isLoading = false
          this.objAddress = result.data
          this.deliveryList = true
        },
        (error) => {
          this.isLoading = false
          console.log(JSON.stringify(error))
          alertify.error('Data ข้อมูลแผนกผิดพลาด');
        }
      )
    },
    openAddCustomer(){
    if(this.idcus == 0){
          Swal.fire({
          title: "คุณไม่ได้เลือกลูกค้า!",
          text: "กรุณาเลือกลูกค้า",
          type: "warning",
          showCancelButton: false,
          confirmButtonColor: "#3085d6",
          confirmButtonText: "ตกลง"
        })
      }else{
        this.contactAdd = true
      }
    },
    searchContact() {
      if(this.idcus == 0){
          Swal.fire({
          title: "คุณไม่ได้เลือกลูกค้า!",
          text: "กรุณาเลือกลูกค้า",
          type: "warning",
          showCancelButton: false,
          confirmButtonColor: "#3085d6",
          confirmButtonText: "ตกลง"
        })
        return
      }
      let payload = {
        id: this.idcus,
        keyword: ""
      }
      console.log(payload)
      this.isLoading = true
      api.searchCustContactByKeyword(payload,
        (result) => {
          this.isLoading = false
          console.log(JSON.stringify(result.data))
          // if (result.data.length == 0) {
          //   alertify.error('ไม่พบผู้ติดต่อ');
          //   return
          // }
          // if (result.data.length > 0) {
          //   this.showContact = true
          //   this.objContact = result.data
          // }
          this.showContact = true
          this.objContact = result.data
        },
        (error) => {
          this.isLoading = false
          console.log(JSON.stringify(error))
          alertify.error('Data ข้อมูลผู้ติดต่อผิดพลาด');
        })
    },
    addContactCust() {
      Swal.fire({
        title: "ยืนยันการทำรายการ?",
        text: "เมื่อยืนยันระบบจะทำการเพิ่มข้อมูลผู้ติดต่อนี้!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes"
      }).then(result => {
        if(result.value){
        let payload = {
          id: 0,
          company_id: 1,
          branch_id: 1,
          ar_id: this.idcus,
          contact_code: this.con_contact_code,
          contact_name: this.con_contact_name,
          address: this.con_address,
          telephone: this.con_telephone,
          email: this.con_email,
          line_id: this.con_line_id,
          line_number: this.con_line_number,
          create_by: this.objuser.sale_code,
        };
        console.log(payload);
        this.isLoading = true;
        api.addCustContact(
          payload,
          result => {
            console.log(result);
            Swal.fire({
              position: "top-end",
              type: "success",
              title: result.result,
              showConfirmButton: false,
              timer: 1500
            });
            this.isLoading = false;
            this.contactAdd = false
          },
          error => {
            this.isLoading = false;
            console.log(JSON.stringify(error));
            alertify.error("Data ข้อมูลแผนกผิดพลาด");
          }
        );
      }});
    },
  },
  mounted() {
    this.idcus = this.$route.params.id
    console.log(this.idcus);
    this.CustomerByID(this.idcus)
  }
};
