import Vue from "vue";
import Loading from "vue-loading-overlay";
import "vue-loading-overlay/dist/vue-loading.css";
import api from "../service/service.js";
Vue.use(require("vue-shortkey"));
export default {
  name: "customerlist",
  props: {},
  components: {
    Loading
  },
  data() {
    return {
      msg: "",
      star: true,
      typing:false,
      Search: "",
      fillter: "doc_no",
      filterby: "DSEC",
      pages: "Me",
      searched: "",
      profile: JSON.parse(sessionStorage.Datauser),
      sale_code: JSON.parse(sessionStorage.Datauser),
      dataall: [],
      keyword_showalldoc: "",
      changefilter: false,
      listitem: [],
      datas: [],
      datalist: [],
      isLoading: false,
      pagehere: "Me",
      page: {
        page: 1,
        rowPerpage: 20
      },
      pageMe: {
        page: 1,
        rowPerpage: 20
      },
      pageOwn: {
        page: 1,
        rowPerpage: 20
      },
      pagePrefit: {
        page: 1,
        rowPerpage: 20
      },time:null,
    };
  },
 watch: {
    searched: function () {
      if(this.tiem == null) {
        clearTimeout(this.time);
        this.time = null
        this.typing = false
      }
      this.time = setTimeout(function(){
         console.log(2)
        }, 300);
      console.log(this.time)
    },typing:function () {
       console.log(4)
    }
  },
  computed: {
    pageME() {
      return this.page.rowPerpage
        ? Math.ceil(this.dataall.length / this.page.rowPerpage)
        : 0;
    },
    pagesleftME() {
      if (this.page.page == 1) {
        return 0;
      }
      return (this.page.page - 1) * this.page.rowPerpage;
    },
    pagesrightME() {
      if (this.page.page == this.pageME) {
        return this.listFilter.length;
      } else {
        return this.page.page * this.page.rowPerpage;
      }
    },
    listFilter() {
      var myNewStr = this.searched.substr(-1);
      var dataall;

      var str = this.searched.split(" ");

      //console.log(this.datalist.length);
      //console.log(this.datalist);
      for (let i = this.datalist.length; i >= str.length; i--) {
        this.datalist.pop();
      }
      //console.log(this.datalist);
      if (myNewStr == " ") {
        this.datalist.push(this.listitem);
        //console.log(this.datalist);
      }
      console.log(str.length);
      if (str.length > 1) {
        //console.log(this.datalist[str.length - 2]);
        this.datas = this.datalist[str.length - 2].filter(dataalls => {
          console.log(1);
          if (
            dataalls.code
              .toLowerCase()
              .includes(str[str.length - 1].toLowerCase()) ||
            dataalls.name
              .toLowerCase()
              .includes(str[str.length - 1].toLowerCase()) ||
            dataalls.telephone
              .toLowerCase()
              .includes(str[str.length - 1].toLowerCase())
          ) {
            return (
              dataalls.code
              .toLowerCase()
              .includes(str[str.length - 1].toLowerCase()) ||
            dataalls.name
              .toLowerCase()
              .includes(str[str.length - 1].toLowerCase()) ||
            dataalls.telephone
              .toLowerCase()
              .includes(str[str.length - 1].toLowerCase())
            );
          }
        });
      } else {
        this.datas = this.dataall.filter(dataalls => {
          console.log(this.dataall);
          if (
            dataalls.code
              .toLowerCase()
              .includes(str[str.length - 1].toLowerCase()) ||
            dataalls.name
              .toLowerCase()
              .includes(str[str.length - 1].toLowerCase()) ||
            dataalls.telephone
              .toLowerCase()
              .includes(str[str.length - 1].toLowerCase())
          ) {
            return (
              dataalls.code
              .toLowerCase()
              .includes(str[str.length - 1].toLowerCase()) ||
            dataalls.name
              .toLowerCase()
              .includes(str[str.length - 1].toLowerCase()) ||
            dataalls.telephone
              .toLowerCase()
              .includes(str[str.length - 1].toLowerCase())
            );
          }
        });
      }
      //console.log(this.dataall);
      if (myNewStr == " ") {
        this.datas = this.listitem;
        //console.log(this.listitem);
        return this.datas;
      } else {
        this.listitem = [];
        this.listitem = this.datas;

        //console.log(this.listitem);
      }
      if (this.filterby == "ASC") {
        return this.datas.sort((a, b) => (a.doc_no > b.doc_no ? 0 : -1));
      } else {
        return this.datas.sort((a, b) => (a.doc_no < b.doc_no ? 0 : -1));
      }
    }
  },
  methods: {
    pagecgange(val) {
      if (val == 1) {
        if (this.page.page < this.pageME) {
          this.page.page += 1;
        }
      } else {
        if (this.page.page != 1) {
          this.page.page -= 1;
        }
        console.log(this.pageMe.page);
        console.log(this.pages);
        return;
      }
    },
    convertToBaht(val) {
      var result = numeral(val).format("0,0.00");
      return result;
    },
    goindex() { //สร้าง user
      this.showNavigation = false;
        this.$router.push({ name: "addcustomer", params: { id: 0 } })
        return;
    },
    seedetail(val) { //แก้ไข user
      //console.log(JSON.stringify(val));
        this.$router.push({ name: "addcustomer", params: { id: val.id } })
        return;
    },
    showlist() {
      this.isLoading = true;
       var payload = {
        keyword: this.keyword_showalldoc
      };
      this.dataall = [];
      api.Customerall(
        payload,
        result => {
          this.isLoading = false;
          console.log(result);
          for (var i = 0; i < result.member.length; i++) {
            this.dataall.push(result.member[i]);
          }
        },
        error => {
          this.isLoading = false;
          console.log(JSON.stringify(error));
          alertify.error("member ข้อมูลค้นหาลูกค้าผิดพลาด");
        }
      );
    }
  },
  mounted() {
    this.showlist();
  }
};
